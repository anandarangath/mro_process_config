"""
Created on 02/05/2016 12:42
Copied on 01/07/2016 13:45
psmropy
@author: Kai Neuhaus
@email: kaineuhaus.mail@gmail.com, kneuhaus2@nuigalway.ie
IDE: PyCharm Community Edition

Usage:
======
see process_config!


Post processing
===============
Call pd.manipulate_img_array( ... )
and modify processing directly in this function.

TODO list:
==========
see at the bottom of source code!

Reasoning:
==========
Dedicated signal analysis module using class concepts to simplify coding.
The copy is based on the linearize_create_lut.
However, even if lots of functions can be kept it is of value to have different class objects
to enable stateful analysis, meaning to have a data analysis status that can be passed along.
Further, we remove the artefacts to be able to process for MRO and focus rather more on to be
able data from MRO.

Explanation for mirror data analysis
====================================
The background of the effort to analyze an assumed known mirror signal is actually that it is not known from the point
of first calibration.
Although, theoretically the mirror signal is known, it is not known how accurate the actual signal follows theory.
Subsequently, it is paramount to find some method to measure and compensate some amount of deviations.

Then it is plausible, that the most accurate simulation of mirror data is not always or not at all of any help, if the
real signal deviates but is not measured based on a more general concept.

A more general concept is to capture besides theoretically predicted characteristics a wider range of parameters.
If one considers ideally, that the relative characteristics between theory $T$ and an assumed ideally behaving system $S$
then it could be called linear based on $C = T/S$ with $C$ describing the systems transfer function.

Currently it appears to be too complex to capture sufficient parameters of the transfer function to obtain a sufficient
model for practical purpose.

A more general measurement model can therefore provide additional data that would be otherwise lost.

Explanation of the measurement model
====================================
The measurement model is based on known driving parameters, such as the frequency of scanning.

The scanning frequency provides immediately predictive boundaries for a template to detect a particular order of reflection,
including its throw.
Although some deviations are to expect, it is then much easier to measure the point of interests by further fitting functions
that do not rely anymore on a global assumption that may otherwise lead to wrong extremas.

Throw vs Frequency
==================
The throw $T$ (T = 2A) which is two times the amplitude is related to the actual frequency of the scanning mirror.
Although, it is not possible directly to extract the spatial dimension of the throw with the time-domain signal, the
stepping distance of multiple mirror reflection can be used to exactly calibrate the time-domain signal to an actual
physical spatial dimension.

The first step is to use the relation that x = A * sin(wt + p) and that d(x) = 1/w A cos(wt + p).
The later function describes the velocity of the mirror at time t.
In this case it is now possible to calculate the maximum speed of the mirror for an given scan frequency and further
the expected travel distance.

The expected travel distance can be used to generate boundary conditions to search for the measured points of returns
in the digitized time-domain signal.


Usage: framework is work in progress.
Additional description will follow soon.

Other subjects
==============
To use sos filter design we need to have a pretty new python version (here 3.5) (see https://www.python.org/downloads/)
If not installed try to obtain the most reason version available.
Assure before compilation that libssl-dev and openssl is installed as otherwis pip will not be installed.
For linux a parallel installation (works also for windows) can be install from www.python.org and
compiled with configure, make, make install see http://askubuntu.com/questions/680824/how-do-i-update-python-from-3-4-3-to-3-5.
To get PyQt (here 5) do pip3.5 install PyQt5.
To get matplotlib compiling assure that libfreetype6-dev is installed beforehand.
Otherwise installing scipy and numpy should be pretty straight forward.
"""

# We import libraries with separate namespace as we anticipate
# further development towards advanced user interface or GUI.
import os
import matplotlib
# import numba

# https://blog.ytotech.com/2015/11/01/findpeaks-in-python/
# The numpy approach is somewhat more elaborate, but also peak detection appears to be more an art.

try:
    from PyQt5 import QtGui, QtCore, Qt
    matplotlib.use('qt5agg')
except ImportError:
    import PyQt4
    matplotlib.use('qt4agg')

import multiprocessing as mp
import matplotlib.pyplot as pp
import numpy as ny
import scipy as sy
import scipy.fftpack as fft
import scipy.signal as sg
import scipy.interpolate as ip
import scipy.optimize as op
import scipy.misc
from scipy.signal import savgol_filter
from scipy.signal import find_peaks
from Research_Functions import detect_peaks

import time
import warnings
import pickle
import glob


# http://dsp.stackexchange.com/questions/2864/how-to-write-lowpass-filter-for-sampled-signal-in-python
# some nice discussion about filters
# http://www.bogotobogo.com/python/OpenCV_Python/python_opencv3_NumPy_Arrays_Signal_Processing_iPython.php
# more about efficient boxcar filters, windowing
# http://stackoverflow.com/questions/27746297/detrend-flux-time-series-with-non-linear-trend
# detrending is close by boxcar filtering of low frequencies.

class FilterTypeNotFound(Exception):
    def __init__(self, str):
        Exception(str)

class FilterType(object):
    """
    The FilterType assumes it is Gauss by default.
    """
    #We serialize those data and save them into intermediate research signal objects.
    #In such a way we might be able to reproduce the signal processing.
    #hdf was not considered here as it is of more value to have the full access to the python object structure.
    name = None
    pb = None
    sb = None
    gbw = None

    def print_params(self):
        if self.name == 'ellip' or self.name == 'cheby2':
            return '{}_pb{}_sb{}'.format(self.name, self.pb, self.sb)
        elif self.name == 'gauss':
            return '{}_BW{}'.format(self.name, self.gbw)
        else:
            raise FilterTypeNotFound('')

class BasePlot(object):
    '''All default stuff to create simple plots.'''

    from PyQt5 import QtGui
    class Options(): pass
    options = Options()
    options.do_break = False
    options.stop_loop = False

    def __init__(self):
        print('Key: (q) quit; (n) next.')
        # pp.rcParams['text.usetex'] = True
        # pp.rcParams['text.latex.preamble']=['\\usepackage{siunitx}']
        pp.rcParams['font.size'] = 20
        # https://stackoverflow.com/questions/11367736/matplotlib-consistent-font-using-latex
        # pp.rcParams['mathtext.fontset'] = 'custom'
        # pp.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
        # pp.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
        # pp.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
        pp.rcParams['mathtext.fontset'] = 'stix'
        pp.rcParams['font.family'] = 'STIXGeneral'
        # pp.rcParams['lines.linewidth'] = 1.0
        # rcParams['lines.markersize'] = 1.0
        # pp.rcParams['font.family'] = 'serif'
        # pp.rcParams['font.serif'] = 'Times'


    def key_pressed(self,evt):
        '''Default key_pressed monitor.
        Use fig.canvas.mpl_connect('key_press_event',self.key_pressed)
        in your function to connect to the this method or overwrite it.'''
        # print(evt.key)
        if evt.key == ('q' or 'Q'):
            self.options.do_break = True
            self.QtGui.QGuiApplication.exit()
            exit()
        if evt.key == ('g' or 'G'):
            self.options.do_break = True
            self.QtGui.QGuiApplication.exit()
        if evt.key == ('c' or 'C'):
            pp.close('all')
            self.options.do_break = True
            self.QtGui.QGuiApplication.exit()

        if evt.key == ('n' or 'N'):
            self.options.do_break = False
            self.QtGui.QGuiApplication.exit()



    def mouse_button_pressed(self, evt):
        if evt.xdata and evt.ydata:
            print('[{:05.2f}, {:05.2f}]'.format(evt.xdata, evt.ydata))

    def wait_for_keys(self):
        '''
        Example of an default implementation of an particular often used plot.
        :param d1:
        :param d2:
        :return:
        '''
        # pp.rcParams['font.size'] = 16
        # fig = pp.figure(figsize=(10,7))
        # fm = pp.get_current_fig_manager()
        # fm.window.setGeometry(50,100,640,550)
        # http://stackoverflow.com/questions/7449585/how-do-you-set-the-absolute-position-of-figure-windows-with-matplotlib
        # fig.canvas.manager.window.move(100, 400)
        # pp.rcParams['font.size'] = 16
        # pp.tight_layout()
        pp.gcf().canvas.mpl_connect('key_press_event', self.key_pressed)
        pp.gcf().canvas.mpl_connect('button_press_event', self.mouse_button_pressed)

        if not self.options.do_break:
            self.QtGui.QGuiApplication.instance().exec()

        return self.options.__dict__

    def show(self, block = False):
        # bring up the plot window and draw but not start event_loop!
        pp.show(block = block)
        # BTW show is also called by pyplot in pause !!!

    def pause(self, delay = None):
        # test if a figure is open
        # matplotlib.pyplot.get_fignums()
        canvas = pp.gcf().canvas
        if canvas.figure.stale:
            pp.show(block = False) # pyplot does the same
        if not delay == None:
            time.sleep(delay)
        # self.QtGui.QGuiApplication.instance().exec()

    def plot_ie(self, d1, d2):
        '''
        Sample implementation of pplot.
        The pplot is special for whatever default plots are rquired.
        So, you should have multiple 'pplots'.
        This means any arbitrary collection of functions tha generate some default plots.
        I.e. you could generate something subplots(n=4, data1, data2, data3, data4) that is generating automatically
        4 supblots.
        :param d1:
        :param d2:
        :return:
        '''
        self.pplot(d1,d2)
        pp.tight_layout()
        # pp.show(block=False)
        pp.draw()
        # pp.waitforbuttonpress()
        # actually pp.waitforbuttonpress implements to call exec(), which loops until a button was pressed.
        self.QtGui.QGuiApplication.instance().exec()


class SignalConfig(BasePlot):
    """
    This class takes the raw signal and attaches all relevant parameters,
    and provides relevant functions to check fundamental parameters.
    Some simple plotting or printing capabilities, dumping and restoring
    can be implemented here.

    Analysis function should be maintained in a separate packgage as those are often growing
    quickly and also change quickly.
    We may possibly envision to provide those as a super class, in which case we could inherit
    from an analysis class for a particular class or set of analysis steps.
    """
    parent_git_version = None
    processing_steps = []
    path = None
    fname = None
    fname_save = None
    max_orders = None
    wavelength = None
    scan_freq = None
    sample_rate = None
    sample_interval = None
    mirror_step_size = None
    scan_range = None
    throw = None
    PMspacing = None
    mroDtype = None
    sample_len = None
    p_sig_start = None
    p_sig_stop = None
    axial_img_res = None
    odisp = None
    pixel_shift = None
    peak_list = None
    sigP = None
    target_samples = None
    filter_type = None
    dbglg = None
    datlg = None
    default_parameter_list = None

    bpp = None

    def __init__(self):
        """Allow to create an empty signal object."""
        # redefine all essential parameters to get them into the dictionar of attributes
        # such that we can list them if required.
        # Sure, we can also just look up the source code, but in cases we want programmatically ask for it.
        # I.e. the member __dict__ will show them all
        self.parent_git_version = None
        self.processing_steps = []
        self.path = None
        self.fname = None
        self.fname_save = None
        self.max_orders = None
        self.wavelength = None
        self.scan_freq = None
        self.sample_rate = None
        self.sample_interval = None
        self.mirror_step_size = None
        self.scan_range = None
        self.throw = None
        self.PMspacing = None
        self.mroDtype = None
        self.mro_segment_len = None
        self.sample_len = None
        self.p_sig_start = None
        self.p_sig_stop = None
        self.axial_img_res = None
        self.odisp = None
        self.pixel_shift = None
        self.peak_list = None
        self.sigP = None
        self.target_samples = None
        self.filter_type = None
        self.dbglg = None
        self.datlg = None
        self.default_parameter_list = None
        self.default_parameter_list = self.__dict__.copy()
        self.bpp = BasePlot()
        
    def build_filenames(self):
        assert self.fname_save is not None, 'Please define sigConf.fname_save!'
        self.name_npy = os.path.join(self.path, self.fname_save+'.npy')
        self.name_pproc = os.path.join(self.path, self.fname_save+'_fr.pkl')
        self.name_sum = os.path.join(self.path, self.fname_save+'_sum.pkl')
        self.name_phs = os.path.join(self.path, self.fname_save+'_phs.pkl')
        self.name_lin = os.path.join(self.path, self.fname_save+'_lin.pkl')
        self.name_flt = os.path.join(self.path, self.fname_save+'_flt.pkl')
        self.name_hlb = os.path.join(self.path, self.fname_save+'_hlb.pkl')
        self.name_rimg = os.path.join(self.path, self.fname_save+'_rimg.pkl')
        self.name_pimg = os.path.join(self.path, self.fname_save+'_pimg.pkl')

    def set_fname_save(self, fname_save):
        self.fname_save = fname_save

    def initSigConf(self, sigconf):
        # todo: Setting config values to the child object is not helping to persist data
        # todo: possibly keeping the sigconfig as member is more explicit
        # print(sigconf.__dict__)
        member_dict = self.__dict__
        for key, val in sigconf.__dict__.items():
            member_dict[key] = val

        self.build_filenames()

    def list_params(self):
        for key, val in self.__dict__.items():
            if (type(val) == list or \
                    type(val) == ny.ndarray) and \
                    not (key in ('peak_list','sigP')):
                pass
            else:
                self.dbglg.info('{} = {}'.format(key,val))
        return self.__dict__

    def log_params(self):
        from collections import OrderedDict as od
        param_dict = od(sorted(self.__dict__.items())).items()
        for key, val in param_dict:
            if key == 'filter_type':
                self.dbglg.info('filter_type = {}'.format(val.__dict__))
            if key == 'default_parameter_list':
                pass
            if (type(val) == list or type(val) == ny.ndarray) and not (key == 'peak_list' or key == 'sigP' ):
                pass
            else:
                self.dbglg.info('{} = {}'.format(key,val))

    def reject_new_parameters(self):
        """Not really rejecting but if new ones occur please consider already used ones."""
        if (set(self.__dict__) - set(self.default_parameter_list  )) :
            self.dbglg.error('Found new parameter {}'.format(set(self.__dict__) - set(self.default_parameter_list)))

    def check_for_missing_parameter(self):
        """
        Not yet implemented.
        This function checks quickly for a minimal set of parameters and returns FALSE if one
        parameter is missing by sending a message to the console.
        :return: BOOL
        """
        self.dbglg.error('Not implemented yet.')
        return False

    def store_processing_state(self, sfname, show_dict = False):
        """
        The pickle is analyzing the dictionary of the class.
        In particular do we strictly store only members that contain small amount of datas
        and everything that is supposed a signal is not exposed but saved in a dedicated
        dictionary part such that it can selectively be stored or deleted and ignored to
        avoid memory overload.

        :param sfname:
        :return:
        """
        tmp_dict = dict()
        # Decompose filter parameter class as it cannot be pickled.
        # So we loop over all members in this class (self) and over the members in filter type and add those
        # to the dictionary to be stored.
        # If we find filter_type then we just save the whole member dictionary, as we know that all those
        # data can be pickled. The function is not required.
        if show_dict: print('self dict: \n', self.__dict__)
        for key, val in self.__dict__.items():
            if key != 'default_parameter_list' and \
                key != 'dbglg' and key != 'datlg':
                # the filter_type attribute is pre-defined and this string must match
                if key == 'filter_type':
                    val = self.filter_type.__dict__.copy()
                # print(key, val)
                tmp_dict[key] = val
            if key in {'fname','myname'}:
                tmp_dict[key] = val

        if show_dict: print('\ntmp_dict: \n', tmp_dict)

        with open(sfname,'wb+') as fid:
            # pickle.dump({'specs':tmp_dict,'forward':self.forward_data,'reverse':self.reverse_data},fid)
            pickle.dump(tmp_dict,fid)

        # deleting data should be performed explicitly and not here.
        # Because in some cases we may still need them and this depends on different facators
        # that are not of general nature.

        # self.signal = None
        # self.forward_data = None
        # self.reverse_data = None
        # del self.signal
        # del self.forward_data
        # del self.reverse_data

    def load_pickle(self,fname):
        with open(fname,'rb+') as fid:
            data = pickle.load(fid)
        return data

    def restore_phase_data(self,sfname):
        data = self.load_pickle(sfname)
        return data['fitted_phase']

    def restore_aline_data(self,sfname):
        data = self.load_pickle(sfname)
        return data['hilbert_data_fw']

    def restore_lin_data(self,sfname):
        data = self.load_pickle(sfname)
        return data['lin_fringe_signals_fw']

    def restore_processing_state(self, sfname, restore_config = False, select_config=None, show_dict = False):
        """

        :param sfname:
        :return:
        """
        with open(sfname,'rb+') as fid:
            data = pickle.load(fid)

        if show_dict: print(data)
        # Create object state with members and filter_type object
        # self.forward_data = data['forward']
        # self.reverse_data = data['reverse']

        if restore_config:
            self.dbglg.info('Restore config from file.')
            member_dict = self.__dict__
            for key, val in data.items():
                if key == 'filter_type':
                    # print('\nfilter_type: ', val)
                    self.filter_type = FilterType()
                    member_dict_ft = self.filter_type.__dict__
                    for key, val in val.items():
                        # print('filter type:',key, val)
                        member_dict_ft[key] = val

                member_dict[key] = val
        elif select_config in {'fringe_signal_phase_sum','fitted_phase'}:
            self.dbglg.info('Restore config {}'.format(select_config))
            member_dict = self.__dict__
            member_dict[select_config] = data[select_config]
        else:
            self.dbglg.info('Keep config from processing.')

    def plot_spectrogram(self, aLine):
        pp.cla()
        # pp.plot(ds)
        fr, tm, spec = sg.spectrogram(aLine, nperseg=1024, noverlap=256)
        pp.subplot(211)
        pp.title('Spectrogram at {:d}'.format(-1))
        pp.pcolormesh(tm, fr, sy.log10(spec))
        pp.ylim((0, 0.1))
        pp.subplot(212)
        pp.plot(aLine)
        pp.title('Raw signal')
        pp.grid()
        pp.show(block=False)
        self.bpp.wait_for_keys()

    def analyze_mro_npy(self, data = None, threshold_mn=1.4, do_plot=False, analyze_data=-1):
        """
        TODO analyze_mro_npy is currently not used.
        DON'T USE!
        This function is currently a bit meaningless and should not be used.
        It is kept here as a place holder for future use but it needs to be re-implemented again.
        Currently the output of this function nil.

        :param block_size_to_analyze:
        :param move_data_window_by:
        :param threshold_mn:
        :return:
        """

        self.dbglg.info('enter')
        if data is None:
            data = self.forward_data
        elif any(data):
            pass

        # self.dbglg.info('shape aline:' + str(sy.shape(data['aline'])))
        # self.dbglg.info('shape frame:' + str(sy.shape(data['frame'])))
        self.dbglg.info('shape data:' + str(sy.shape(data)))


        for segment,i in zip(data,range(data.shape[1])):
            segment = segment - sy.mean(segment)

            sig_max = ny.max(data)
            self.dbglg.info('max data: '+str(sig_max))
            freq, time, spec = sg.spectrogram(segment,
                                              nperseg=1024*5,
                                              noverlap=1024*4.5)
            # freq1, time1, spec1 = sg.spectrogram(sy.diff(segment, 2))  # ,nperseg=64,noverlap=64/8)

            # spec1 = sy.log10(spec1)
            spec_log = sy.log10(spec)

            self.dbglg.info('spec shp: '+str(sy.shape(spec)))

            # do_plot = False
            if do_plot and i>=analyze_data:
                pp.figure(num='spec',tight_layout=True,figsize=(9,7))

                # def disabled():
                ax = pp.subplot(211)
                pp.cla()
                trng = sy.linspace(0,len(segment)/20e3,len(segment))
                pp.plot(trng,segment/segment.max())

                pp.title('Raw buffer content')
                pp.ylabel('Amplitude (arb.)')
                pp.xlabel('Acq. time (ms)')

                pp.subplot(212)
                pp.cla()

                # pp.pcolor(time,freq,spec)
                # cm = pp.pcolormesh(time,freq,spec_log,vmin=4,vmax=15,cmap='CMRmap',interpolation='bicubic')
                # print(spec_log)

                cm = pp.imshow(sy.flipud(spec_log),
                               cmap='CMRmap',
                               extent=[0,trng.max(),0,6],
                               # aspect=64476*2/3,
                               aspect=2,
                               vmin=4,vmax=15)
                cb1 = pp.colorbar(cm, fraction=0.046, pad=0.04)
                cb1.set_label('Intensity (log$_{10}$ scale)')
                pp.title('Spectrogram of signals')
                pp.xlabel('Acq. time (ms)')
                pp.ylabel('Frequency (MHz)')
                pp.ylim((0,0.5))
                # pp.grid()
                # pp.draw()
                pp.tight_layout()
                pp.pause(0.1)
                # path = '/home/kai/Documents/00_00_Thesis/thesis_v0-0-0-1_bitbucket/ch5_conclusion/ch5_images/'
                # print(self.fname_save)
                # pp.savefig(self.fname_save + '-non-Lin-s{:02}.pdf'.format(i))
                while pp.waitforbuttonpress() == False: pass
                cb1.remove()
                # cb2.remove()
        while pp.waitforbuttonpress() == False: pass



class PreprocessSignal(SignalConfig):
    def __init__(self, sigconf):
        # The data from the static definition are not propagated through.
        # In some way we just simulate a more convinient dictionary here,
        # but having the items as class members and still are able to add or remove some
        # for storage and saving memory.
        self.initSigConf(sigconf)

    def make_timeStamp(self):
        return time.strftime('%Y-%m-%d-%H-%M-%S')

    def add_colorbar(self, im, aspect=20, pad_fraction=0.5, **kwargs):
        """Add a vertical color bar to an image plot.
        http://stackoverflow.com/questions/18195758/set-matplotlib-colorbar-size-to-match-graph
        """
        from mpl_toolkits import axes_grid1
        divider = axes_grid1.make_axes_locatable(im.axes)
        width = axes_grid1.axes_size.AxesY(im.axes, aspect= 1/ aspect)
        pad = axes_grid1.axes_size.Fraction(pad_fraction, width)
        current_ax = pp.gca()
        cax = divider.append_axes("right", size=width, pad=pad)
        pp.sca(current_ax)
        return im.axes.figure.colorbar(im, cax=cax, **kwargs)


    def plot_mro_data(self, start_plot):
        plt = BasePlot()
        fig = pp.figure('MRO Data')
        pp.show(block=False)
        for fd, rd, cnt in zip(self.forward_data, self.reverse_data, range(len(self.forward_data))):
            if cnt >= start_plot:
                pp.clf() # to avoid deprecation warning
                pp.subplot(121)
                # pp.cla()
                pp.plot(fd,linewidth=0.5)
                pp.title('Forward {}'.format(cnt))

                pp.subplot(122)
                # pp.cla()
                pp.plot(rd,linewidth=0.5)
                pp.title('Reverse')

                pp.draw()
                # self.pause()
                pp.tight_layout()
                if plt.wait_for_keys()['do_break']: break

    def analyze_mro_bin(self, block_size_to_analyze=18000, move_data_window_by=2000, threshold_mn=1.4, do_plot=False):
        """
        Try to find the structure of a continuous binary stream based on some
        assumptions of an acquired data file for MRO.
        No data will be saved here.
        :param block_size_to_analyze:
        :param move_data_window_by:
        :param threshold_mn:
        :return:
        """

        self.dbglg.info('enter')
        data = ny.fromfile(self.path + self.fname, dtype=ny.dtype([('all', self.mroDtype )]))
        self.dbglg.info('Found type: '+str(type(data)))
        # self.dbglg.info('shape aline:' + str(sy.shape(data['aline'])))
        # self.dbglg.info('shape frame:' + str(sy.shape(data['frame'])))
        self.dbglg.info('shape data:' + str(sy.shape(data['all'])))

        fig1 = pp.figure('Spectrogram', figsize=(13,10))

        for shift in range(0,len(data),move_data_window_by):
            segment = data['all'][shift:block_size_to_analyze+shift]
            segment = segment - sy.mean(segment)

            sig_max = ny.max(data['all'])
            self.dbglg.info('max data: '+str(sig_max))
            freq, time, spec = sg.spectrogram(segment) #nperseg=128,noverlap=128/8)
            freq1, time1, spec1 = sg.spectrogram(sy.diff(segment, 2))  # ,nperseg=64,noverlap=64/8)
            spec1 = sy.log10(spec1)
            spec_log = sy.log10(spec)

            self.dbglg.info('spec shp: '+str(sy.shape(spec)))
            # self.dbglg.info('spec content: '+str((spec[50])))
            spec_log_mnt = sy.mean(spec_log)
            avg_of_peak_line = range(0,50)
            spec_log_mn = sy.mean(spec_log[avg_of_peak_line],axis=0)
            pk_idx = sy.where(spec_log_mn>spec_log_mnt*threshold_mn)
            self.dbglg.info('spec mnt: '+str(spec_log_mnt)+' > '+str(threshold_mn))
            self.dbglg.info('spec pks: '+str(pk_idx))
            pk_smpl = sy.array(pk_idx / ny.max( sy.shape(spec)[1] ) * len(segment))
            self.dbglg.info('spec pks: '+str(pk_smpl))
            pk_smpl_df = sy.diff( pk_smpl )
            self.dbglg.info('spec dffs: '+str(pk_smpl_df ))
            if len(pk_smpl_df[0])>2:
                pk_smpl_df_med = sy.median( pk_smpl_df)
                self.dbglg.info('spec median: '+str(pk_smpl_df_med))

            # do_plot = False
            if do_plot:

                # apply different stages of derivatives
                # the spectrogram is acting as a universal filter but may be slow for general
                # filtering. Nevertheless is well suited for detecting boundaries.
                # create a segmented 2D array for further processing.
                pp.subplot(312)
                pp.cla()

                # pp.pcolor(time,freq,spec)
                cm = pp.pcolormesh(time,freq,spec_log)
                cb1 = pp.colorbar(cm, fraction=0.046, pad=0.04)
                cb1.set_label('Intensity (log$_{10}$ scale)')
                pp.title('Spectrogram of signals')
                pp.xlabel('Buffer length (samples)')
                pp.ylabel('Frequency (normalized)')
                pp.grid()
                pp.draw()

                pp.subplot(313)
                pp.cla()
                # pp.pcolor(time,freq,sy.log10(spec))
                cm = pp.pcolormesh(time1,freq1,spec1)
                cb2 = pp.colorbar(cm, fraction=0.046, pad=0.04)
                cb2.set_label('Intensity (log$_{10}$ scale)')
                pp.title('Spectrogram of signals 2$^{nd}$ derivative')
                pp.xlabel('Buffer length (samples)')
                pp.ylabel('Frequency (normalized)')
                pp.grid()
                pp.draw()

                pp.tight_layout()

                ax = pp.subplot(311)
                pp.cla()
                cont = pp.pcolormesh(time,freq,spec) # just to get a usable creator for the colorbar
                pp.plot(segment)

                pp.title('Raw buffer content')
                pp.ylabel('Amplitude (samples)')
                pp.xlabel('Buffer length (samples)')
                # create colorbar with lable to scale the plot like all the other plots
                cbx = pp.colorbar(cont, fraction=0.046, pad=0.04)
                cbx.set_label('Intensity (log$_{10}$ scale)')
                cbx.remove() # then remove it again
                pp.grid()


                self.pause()
                cb1.remove()
                cb2.remove()


    def convert_mro_bin(self, bin_file, a_line_len, offset, do_plot=False, start_plot=0):
        """
        The bin file obtained from the LabView acquisition needs to be converted with this function.

        This function therefore assures the number of A-lines that must be given as **nr_a_lines**.

        We also need to give the length of one A-line as **a_line_len**.

        Eneabling to plot with **do_plot = True** may help to obtain a preview and if the conversion
        produces plausible results, and **start_plot** allows to start plotting at the **N * a_line_len**
        element.

        :param bin_file:
        :param a_line_len:
        :param do_plot:
        :param start_plot:
        :return:

        Instruction to align a true signal.

        A true signal should contain at least one well visible order.

        It may well be that after moving the sample or a different sample the alignment needs to be repeated.

        It is better to align the signal already during acquisition.

        First establish that at position around 5, the wave form is aligned by adjusting the offset.

        Then check at position 90 or even 100 and measure the shift and divide by 100.

        If the shift is positive, then the signal is longer and the result must be subtracted or versa vice.
        """
        self.mro_segment_len = a_line_len
        # The buffer header has usually the dimensions of the data set.
        # This could go wrong sometimes.
        self.dbglg.warning('A_line_len: '+str(self.mro_segment_len))
        self.dbglg.warning('Did you adjust those values according to the numbers in the ML code?')
        fname = bin_file
        self.dbglg.info('load: '+fname)

        with open(fname,'rb+') as fid:
            fid.seek(offset * 2)
            print(self.mroDtype)
            data = ny.fromfile(fid, (self.mroDtype, self.mro_segment_len))
           # ([('offset', self.mroDtype, 5000)],[('S1', self.mroDtype, self.mro_segment_len)]))
           # ('S2', self.mroDtype, self.amount_of_alines)]))

        self.nr_of_alines = sy.shape(data)
        self.dbglg.info('shape: '+str(self.nr_of_alines))
        # self.dbglg.info('shape: '+str(sy.shape(data['S1'])))
        if do_plot:
            fig = pp.figure('Convert MRO.bin')
            # pp.show(block=False)
            print('Key: (n) for next, (q) for quit.')
            count = 0
            for ds in data:
                count += 1
                if count >= start_plot:
                    pp.clf()
                    # pp.plot(ds)
                    # fr, tm, spec = sg.spectrogram(ds,nperseg=1024,noverlap=256,mode='complex') # for 20000 samples
                    fr, tm, spec = sg.spectrogram(ds,nperseg=512,noverlap=128,mode='complex') # for 10000 samples

                    ax = pp.subplot(211)
                    # pp.title('Spectrogram at {:d}'.format(count))
                    ax.set_title('Spectrogram at {:d}'.format(count))
                    pm = pp.pcolormesh(tm, fr, ny.log10(abs(ny.real(spec))))
                    # self.add_colorbar(pm, aspect=0.1, pad_fraction=10.1, cax=ax)
                    # pp.ylim((0,0.05))

                    # pp.subplots_adjust(right=0.8)
                    # ax = pp.gcf().add_axes([0.85, 0.5, 0.03, 0.4])
                    # pp.colorbar(pm,cax=ax)

                    ax2 = pp.subplot(212)
                    ax2.plot(ds)
                    ax2.set_title('Raw signal')
                    ax2.grid()
                    pp.pause(interval=0.1)
                    # if count < 1:
                    pp.tight_layout()
                    self.bpp.wait_for_keys()

        sfname = os.path.join(self.path, self.fname_save+'.npy')
        self.dbglg.info('save mro data into : '+ sfname)
        sy.save(sfname , data)
        return self

    def load_mro_data(self, fname = None, do_plot=False, start_plot=0, resample=False, analyze_data=-1):
        """
        Create files with buffer segments containing forward and reverse scans.
        In future processing the data can be kept in memory to speed up things,
        and storing the data is merely important for research.
        :param fname ... leave this empty it is not used at the moment.
        :param do_plot: True / False
        :param plot_what: 'all', 'forward', 'reverse'
        :return:
        """
        self.reject_new_parameters()
        assert self.path != None, 'Please set a *path* to the folder with data.'
        assert self.fname_save != None, 'Please set a filename *fname* with data.'

        self.dbglg.info('load: '+ self.name_npy)
        self.signal = sy.load(self.name_npy)
        self.dbglg.info('data shape: '+str(sy.shape(self.signal)))

        self.amount_of_alines = sy.shape(self.signal)[0]
        self.dbglg.warn('Using first element to determine #amount_of_alines: '+str(self.amount_of_alines))
        assert self.sigP != None, 'Please set the self.signal boundaries *sigP*! I.e. sigP = [7559, 73129, 138590]'
        self.dbglg.info('self.signal boundaries: '+str(self.sigP))
        self.dbglg.info('Seg length forward: {}'.format(self.sigP[1] - self.sigP[0]))
        self.dbglg.info('Seg length reverse: {}'.format(self.sigP[3] - self.sigP[2]))

        self.forward_data = []
        self.reverse_data = []
        self.nr_of_alines = 0

        for aline in self.signal:
            self.forward_data.append( sy.array(aline[self.sigP[0]:self.sigP[1]]))
            self.reverse_data.append( sy.array(aline[self.sigP[2]:self.sigP[3]]))
            self.nr_of_alines = self.nr_of_alines + 1

        self.dbglg.info('Found {} A-lines.'.format(self.nr_of_alines))
        self.forward_data = sy.array(self.forward_data)
        self.reverse_data = sy.array(self.reverse_data)
        # self.forward_data = [sg.resample( aline[self.sigP[0]:self.sigP[1]], self.target_samples ) for aline in fringe_data]
        # self.reverse_data = [sg.resample( aline[self.sigP[1]:self.sigP[2]], self.target_samples ) for aline in fringe_data]

        self.dbglg.info('save forward and reverse data seprate in: {}'.format(self.name_pproc))
        # sy.save(sfname, {'forward':self.forward_data,'reverse':self.reverse_data})

        if do_plot: self.plot_mro_data(start_plot=start_plot)
        assert analyze_data < 0, 'The option analyze_data is currently not implemented. Please use analyze_data = -1 or don\'t specify at all'
#        if analyze_data>-1: self.analyze_mro_npy(do_plot=True, analyze_data=analyze_data)

        if type(resample) == int:
            # todo: resampling / downsampling at this point is questionable due to filtering problems
            # but it may have some other use.
            self.resample_mro_data( new_sample_rate = resample )
        # delete the current signal data to avoid memory overload
        del self.signal
        # store the configuration data
        self.store_processing_state(self.name_pproc)
        return self


    def resample_mro_data(self, new_sample_rate):
        assert type(new_sample_rate) == int, \
            'For resampling an integer value is required or disable by setting it False!'
        self.dbglg.error('Resampling at this stage can cause filter artefacts!')
        self.target_samples = new_sample_rate
        self.dbglg.info('Start resampling with: '+str(self.target_samples))
        self.dbglg.warn('No window is applied!')

        raise Exception('Resampling needs to be modified to deal with large data.')
        fw_data = sg.resample(self.forward_data, self.target_samples)
        rv_data = sg.resample(self.reverse_data, self.target_samples)
        self.processing_steps.append([self.make_timeStamp, 'resample'])

    def find_point_of_returns(self, do_plot=False, fname=None):
        """
        **Please do not trust the returned value blindly!**

        **fname** is a python binary file '<name>.npy' which should have been created by the
        *convert_mro_bin* function or does come directly from an Python acquisition.

        Also, the values should be modified if the FFT and Hilbert conversions appear to be slow,
        which is caused if the data segment length is not a multiple of 2.

        :param do_plot:
        :param fname:
        :return:
        """
        self.dbglg.info('load file: '+fname)
        data = sy.load(fname)
        self.dbglg.info('data shape: '+str(sy.shape(data)))

        segment = []
        correlation_sum = sy.zeros(len(data[0])-1)

        if do_plot: figp = pp.figure()
        for segment in data:

            segment = segment - sy.mean(segment)

            # The shape is used to find a best correlation in the signal.
            # the parameters were obtained experimentally and work best with the resampling
            # of the signal below.
            shape_samples = len(segment)/500
            shape_beg = -1*sy.pi
            shape_end =  1*sy.pi
            wt = sy.linspace(shape_beg, shape_end, shape_samples)
            ph = -.5*sy.pi
            A = 100
            # shape = A * sy.sin(wt + ph ) + 6000
            shape = -sy.exp(-((0-wt)**2)/.5) + 6000
            segment_hilbert = abs(sg.hilbert(segment))
            # pp.subplot(311)
            # pp.cla()
            # pp.plot(segment_hilbert)
            # The correlation runs over and creates twice the length so we must select
            # the region of interest with [0:len(segment)]
            correlation_vs_x = sy.diff(sg.correlate(segment_hilbert, shape)[0:len(segment)])
            correlation_sum += correlation_vs_x
            # pp.subplot(312)
            # pp.cla()
            # pp.plot(correlation_sum)
            #
            # pp.subplot(313)
            # pp.plot(shape)
            # pp.draw()
            # self.pause()

        peaks = [] # we expect only three peaks anyway
        skip_width = len(segment)*0.0025 # ignore the first pixels due to imperfect correlation with large amplitudes
        for part in range(3):
            win_beg = int(skip_width + len(segment) * part/3)
            win_end = int(len(segment)* (part+1)/3)
            min_in_window = ny.min(correlation_sum[ win_beg : win_end ])
            # we select the first minima detected. If there are more then something is anyway odd.
            all_min_idx = sy.where( correlation_sum == min_in_window )
            if len(all_min_idx) > 1:
                self.dbglg.warn('Detected more than one minima: '+str(all_min_idx))
            peaks.append( all_min_idx[0][0] )

        self.dbglg.warning('Re-run with these values again! '+str( peaks))

        self.iso_thresh_high = 25.0
        self.iso_thresh_low = 0.0
        if do_plot:
            bpp = BasePlot()
            fig1 = pp.figure('Segmentation of buffer')
            for segment in data:
                pp.subplot(311)
                pp.cla()
                pp.plot(segment)
                for p in peaks:
                    pp.plot([p,p],[ny.min(segment),ny.max(segment)])
                pp.grid()
                pp.title('Signal and segmentation')

                pp.subplot(312)
                pp.cla()
                pp.plot(shape)
                pp.grid()
                pp.title('Shape pattern used for correlation')

                pp.subplot(313)
                pp.cla()
                pp.plot(correlation_sum)
                pp.grid()
                pp.title('Sum of correlations')

                pp.tight_layout()
                pp.show(block=False)
                bpp.wait_for_keys()
        return self


class Linearize(SignalConfig):

    bpp = None
    def __init__(self, sigConf):
        # load the current configuration
        self.initSigConf(sigConf)
        self.bpp = BasePlot()

    def pass_data_directly(self):
        '''If we do not intent to linearize the data this function is passing through
        the data as is.'''
        # load pre-processed data
        self.dbglg.info('load: {}'.format(self.name_pproc))
        # self.restore_processing_state(self.name_pproc)
        self.dbglg.info('forward_data: {}'.format(str(sy.shape(self.forward_data))))

        lin_fringe_signals = self.forward_data
        self.lin_fringe_signals_fw = lin_fringe_signals

        # reduce fringe signals according to sigFrom to sigLen
        self.dbglg.info('signal shape : ' +str(sy.shape(lin_fringe_signals)))
        assert len(lin_fringe_signals) > 0, 'This data set appears to be empty. Please check if acquisition was successful.'

        self.dbglg.info('save lin data as: '+self.name_lin)
        del self.forward_data

        self.store_processing_state(self.name_lin)
        return self


    def sum_signals(self, step_mode=False, do_plot=False):
        """
        Summing multiple interference signals.
        If step_mode = True, then wait for each plot with buttonpress to observe each step
        of the summation.
        To reverse the "signal motion" in time or space, which may be required to please the current MRO processing, in case
        the data were recorded the opposite way around.

        The continuity of the instantaneous phase is preserved as long as the signal is continuous.
        Currently the continuity is not assured by the processing and must be manually enforced.
        :param include_reverse:
        :param step_mode:
        :param do_plot:
        :return:
        """

        self.restore_processing_state(self.name_pproc)
        self.dbglg.warn('Using self.forward_data!')
        fringe_signal = self.forward_data
        self.dbglg.warn('Remove reverse data for the moment.')
        del(self.reverse_data)

        self.dbglg.info('Data structure: '+str(sy.shape(fringe_signal)) )

        fringe_signal_phase = None

        sumRaw = sy.zeros(len(fringe_signal[0]))

        limIsSet = {} # for plotting to keep adjusted ylim

        def keepLim(lims, type, keepMax=False):
            # global limIsSet
            if limIsSet.get(type) and keepMax:
                minV, maxV = limIsSet[type]
                if minV > lims[0]:
                    limIsSet[type] = (lims[0], maxV)
                if maxV < lims[1]:
                    limIsSet[type] = (minV, lims[1])
            if limIsSet.get(type):
                return limIsSet[type]
            else:
                limIsSet[type] = lims
                return lims

        self.dbglg.info('create sum signal and phase.')
        self.dbglg.info('psigStart/Stop: (' + str(self.p_sig_start) + ',' + str(self.p_sig_stop) + ')')
        # sig_rng =  sy.arange(self.p_sig_start, self.p_sig_stop + 1)
        sig_rng = self.p_sig_use
        sigs = fringe_signal[sig_rng]

        for sig, at in zip(sigs, sig_rng):
            # print('sum with sig at: ',at)
            sumRaw = sumRaw + sig
            sumNoDC = sumRaw - sy.mean(sumRaw)
            # print('data shape ',sy.shape(sumNoDC))
            # base_line = sy.ones(len(sig))*sy.mean(sig)
            # sumHilbert = sy.absolute(sg.hilbert(sumNoDC))
        sumHilb = sg.hilbert( sumNoDC )

        self.fringe_signal_phase_sum = ny.unwrap(ny.angle(sumHilb))

        # todo: the deletion process of intermediate data is still work in progress
        # todo: It is not yet clear if it is better to perform deletion by different means.
        # remove raw data elements as we only want to store the sum data!
        # if self.__dict__.get('signal'): del self.signal
        # if not None == self.__dict__.get('reverse_data'): del self.reverse_data

        self.store_processing_state(self.name_sum)

        if do_plot:
            if step_mode:
                print('\npress button or ''q'' to quit')

            sumRaw = sy.zeros(len(fringe_signal[0]))
            pp.figure('Sum signals',figsize=(10,7))
            pp.show(block=False)
            for sig, at in zip(sigs, sig_rng):
                print('Data step pos: {}'.format(at))
                sumRaw = sumRaw + sig
                sumNoDC = sumRaw - sy.mean(sumRaw)
                base_line = sy.ones(len(sig)) * sy.mean(sig)
                sumHilbert = sy.absolute(sg.hilbert(sumNoDC))
                fringe_signal_phase = ny.unwrap(ny.angle(sg.hilbert(sumNoDC)))

                pp.subplot(221)
                pp.cla()
                pp.plot((sig - sig.mean()))
                # pp.plot((base_line), '.', ms=5)
                # ylim = keepLim(pp.ylim(), 'sig')
                # pp.ylim([-40000, 40000])
                pp.title('Raw Signal at {:g}'.format(at))

                pp.subplot(222)
                pp.cla()
                pp.plot(sumNoDC)
                # pp.ylim(keepLim(pp.ylim(), 'sumNoDC', keepMax=True))
                pp.title('Sum signal DC removed')

                pp.subplot(223)
                pp.cla()
                pp.plot(sumHilbert)
                # pp.ylim(keepLim(pp.ylim(), 'sumHilbert', keepMax=True))
                pp.title('Hilbert')

                pp.subplot(224)
                pp.cla()
                pp.plot(fringe_signal_phase)
                # pp.ylim(keepLim(pp.ylim(), 'phase', keepMax=True))
                pp.title('Phase')
                pp.tight_layout()
                pp.draw()
                bpp = BasePlot()
                if step_mode:
                    bpp.wait_for_keys()
                else:
                    pp.pause(0.1)
        return self

    def fit_phase(self, target_samples=None, do_plot=True):
        '''target_samples can be set to 5000 as required for Roshan's framework.
        Otherwise all samples are used.
        '''
        self.dbglg.info('load: '+self.name_sum)
        self.restore_processing_state(sfname = self.name_sum, select_config='fringe_signal_phase_sum')
        fringe_signal_phase_sum = self.fringe_signal_phase_sum

        # if no target_samples are given we use all samples
        if not target_samples:
            ts = len(fringe_signal_phase_sum)
            self.dbglg.info('PSMRO mode. Full sample length: ' + str(ts))
        else:
            ts = target_samples
            self.dbglg.info('MRO mode. Limit to sample length: '+str(ts))
            # todo:2 MRO mode sample limit may collide with later resampling.
            # This is minor currently as long as we do not process MRO data.

        # capture original phase length and sample amount
        len_phase = len(fringe_signal_phase_sum)
        org_rng = sy.arange(0, len_phase)

        # generate the target sample space for LUT (reduced samples)

        fit_rng_ts = ny.linspace(0, len_phase - 1, ts).round()
        # fit_rng_ts = ny.linspace(0, len_phase - 1, ts, dtype=int)
        # new_rng = sy.arange(0, ts)

        # phase_interp = ip.interp1d( org_rng, psmro_phase, kind='cubic' )

        # downsample LUT
        # rng_interp = ip.interp1d(org_rng, org_rng)
        # down_phase = sg.resample(self.fringe_signal_phase, ts, window='blackman')  # ('gauss',100))

        # pp.plot( rng_interp(sp.linspace(0,len_phase-1,ts,dtype=int)), phase_interp(fit_rng_ts),'o')

        def fit_func(p, x):
            '''The function we want to fit.'''
            A = p[0]
            freq = p[3] * sy.pi
            phase = p[2]
            DC = p[1]
            return A * sy.sin(freq * x / max(x) + phase) + DC

        # def fit_func(p, x):
        #     A = p[0]
        #     B = p[1]
        #     C = p[2]
        #     D = p[3]
        #     E = p[4]
        #     F = p[5]
        #     return A + B * x + C * x**2 + D * x**3 #+ E * x**4 + F * x**5

        def err_func(p, x, y):
            '''What we want to minimize.'''
            return y - fit_func(p, x)

        # todo:0 Replace the magic numbers for the guess to fit the phase
        #
        p_init = [0, 0, -sy.pi, 1.0]  # initial guess for A, DC, phase, freq
        # p_init = [0, 1, 1, 1, 1, 1] # A, B, C

        if do_plot:
            f2 = pp.figure('fitting')
            pp.ion()
            # f2.canvas.mpl_connect('close_event',my_close_handler)

        res = op.leastsq(err_func, p_init, args=(org_rng, fringe_signal_phase_sum), full_output=True)
        self.dbglg.info('Fitting results: {}'.format(res[0]))

        sav_gol_data = sg.savgol_filter(fringe_signal_phase_sum,window_length=2*1000-1 ,polyorder=5)
        self.dbglg.info('Savitzky_Golay!')
        if do_plot:
            # plot for interpolation. But there are always some oscillations.
            # pp.plot( rng_interp( (sy.linspace(0,len_phase-1,ts ))), down_phase ,'o')
            pp.plot(fringe_signal_phase_sum, label='Phase')
            # pp.plot(fit_func(res[0], org_rng), label='fitted')
            pp.plot(sav_gol_data)
            pp.title('Fitting and Phase')
            pp.legend(loc=2)
            pp.tight_layout()

        fitted_phase = fit_func(res[0], fit_rng_ts)
        fitted_phase = sav_gol_data
        # todo:1 fit_phase: removing neg values may be indication of bad linearization capabilities
        fitted_phase = fitted_phase - min(fitted_phase)  # remove negative values as the ML MRO processing may not be able to deal with them.
        fitted_phase = fitted_phase / max(fitted_phase)  # normalize to 1.0

        self.fitted_phase = fitted_phase
        self.dbglg.info('fitted_phase shape: ' + str( sy.shape(fitted_phase)))

        bpp = BasePlot()
        if do_plot:
            pp.show(block=False)
            print('press ''n'' for next or ''q'' to stop!')
            bpp.wait_for_keys()
            pp.close('all')

        self.dbglg.info('save fitted phase to: '+self.name_phs)
        self.store_processing_state(self.name_phs,show_dict=True)
        return self

    def linearize_data(self, do_plotSteps=False, load_fitted_phase = None, do_analyze=-1):
        """
        Use fitted phase data to linearize signals in fringe_data.
        The parameter **load_fitted_phase** should be a file with linearization data or **None**.
        If the parameter is not given or **None** the current data are assumed mirror data and used for
        linearization.
        *The sample length should not matter in the future anymore and become a configuration parameter,
        meaning the sample length of the phase is merely for quality and speed.*

        :param do_plotSteps:
        :param load_fitted_phase:
        :return:
        """

        # load pre-processed data
        self.dbglg.info('load: {}'.format(self.name_pproc))
        # self.restore_processing_state(self.name_pproc)
        self.dbglg.info('forward_data: {}'.format(str(sy.shape(self.forward_data))))

        fringe_signals = self.forward_data
        # reduce fringe signals according to sigFrom to sigLen
        self.dbglg.info('signal shape : ' +str(sy.shape(fringe_signals)))

        # load phase data
        if load_fitted_phase is None or load_fitted_phase == '':
            self.dbglg.info('load phase data: '+self.name_phs)
            self.restore_processing_state(self.name_phs,show_dict=True,select_config='fitted_phase')
            # self.fitted_phase = self.restore_phase_data(self.name_phs)
            fitted_signal_phase = self.fitted_phase
        else:
            self.restore_processing_state(load_fitted_phase, select_config='fitted_phase')
            self.dbglg.info('Load phase from {}'.format(load_fitted_phase))
            fitted_signal_phase = self.fitted_phase
            self.dbglg.info('phase data shape: {}'.format(sy.shape(self.fitted_phase)))


        self.dbglg.info('phase data: {}'.format(self.fitted_phase))

        assert len(fringe_signals) > 0, 'This data set appears to be empty. Please check if acquisition was successful.'
        fringe_signal_len = len(fringe_signals[0])
        fringe_samples = sy.arange(0, fringe_signal_len)
        # scale values in signal phase based on the length of the signal slice sigFrom and sigLen
        phase = fitted_signal_phase * fringe_signal_len

        # the length of phase samples is expected to be equal to the amount of signal samples.
        # if they are different then a warning is required.
        if len(phase) != fringe_signal_len:
            self.dbglg.warning('Length phase and fring signal differ {:g} != {:g}!'.format(len(phase),fringe_signal_len))
            # resample phase for the signal samples! We do not use resample to avoid window artifacts, and
            # besides of that the phase signal is a low frequency shape anyway, so linear interpolation is sufficient.
            interpolate_phase = ip.interp1d( range( len(phase) ), phase)
            phase = interpolate_phase( sy.linspace( 0, len(phase)-1 , fringe_signal_len))

        # prepare array for linearized signals
        lin_fringe_signals = sy.zeros(sy.shape(fringe_signals))
        idxrng = range(fringe_signal_len)

        if do_plotSteps:
            pp.figure('linearize_data', figsize=(20, 10), tight_layout=True)
            # position needs to acces figure manager.

        # new index space for linear target array / list to have an linear assignment base for the interpolation.
        # The interpolation does basically assign new values to all target bins in the linear array.
        # iphase = sy.ceil( phase * (self.sigTo-self.sigFrom) / max(phase))  # match to max index value from source

        # It must be taken care that the constructed index values or signal values increase.
        # If in the interpolation occur sequences sucha as [0,0,...] vs [11,11,...] then a [nan] value
        # will occur.
        iphase = phase / max(phase) # normalize 0 ... 1.0
        # scale values to index values of the signal range
        iphase = sy.ceil(iphase * fringe_signal_len).astype(int)
        # set min value to zero to obtain first index = 0
        iphase = iphase - min(iphase)
        # find values of zero as those will fail during interpolation
        [zeros_in_iphase] = sy.where( iphase == 0)
        # the last index of the set of zeros
        zero_max_idx = ny.max( zeros_in_iphase)
        print(zeros_in_iphase)
        # set all values before the min point to zero as well, as the x_new range must be continuosly increasing
        iphase[ 0 : zero_max_idx ] = sy.zeros( zero_max_idx )
        raised_NAN = False
        rec_WRN = True
        self.dbglg.info('Remove DC.')
        spec_prev = None
        cb1 = None
        for sig, idx in zip(fringe_signals, idxrng):

            # remove DC
            sig = sig - ny.mean(sig)

            # interpolate over the non-linear iphase_data which is essentially the non-linear phase
            # and assign the bins of the non-linear signal, which should assign it to the non-linear phase bins,
            # effectively cancelling the non-linearity, making it linear.
            lin_signal = None
            with warnings.catch_warnings(record=True) as w:
                warnings.simplefilter('always')
                interpolate_signal = ip.interp1d(iphase, sig)
                lin_signal = interpolate_signal(fringe_samples)
                if rec_WRN and len(w) > 0:
                    self.dbglg.warning([wm.__str__() for wm in w])
                    self.dbglg.warning(
'\n\n*****\nIf multiple values in fringe_samples are the same value or zero\n \
then the slope is zero and divisin by zero occurs.\n \
This will cause some undefined values (nan).\nSee source code. \n*****\n')
                    rec_WRN = False

            if sy.isnan(sum(lin_signal)) and not raised_NAN:
                raised_NAN = True
                self.dbglg.warning('Found nan in lin_signal and try to fill it.')
            lin_signal[sy.where(sy.isnan(lin_signal))] = 0

            # assign values to new data array
            lin_fringe_signals[idx] = lin_signal

            if do_plotSteps and (idx >= do_analyze):
                lin_signal_w = lin_signal #* sg.tukey(len(lin_signal),alpha=1.0)

                pp.subplot(221)
                pp.cla()
                scaling = 1e4
                trng = sy.linspace(0,len(sig)/self.sample_rate * scaling,len(sig))
                pp.plot(trng,sig/sig.max())
                # pp.plot(self.apply_boxcar_td(sig,order=False), '.', ms=5)
                pp.title('Non-Linear Signal')
                pp.ylabel('Intensity (arb.)')
                pp.xlabel('Acq. time (ms)')

                pp.subplot(222)
                pp.cla()
                pp.plot(trng,(lin_signal_w/lin_signal_w.max()), label='lin')
                # pp.ylim([-20000,20000])
                pp.title('Linearized signal')
                pp.ylabel('Intensity (arb.)')
                pp.xlabel('Acq. time (ms)')

                # pp.subplot(223)
                # pp.cla()
                # pp.plot(sy.unwrap(sy.angle(sg.hilbert(lin_signal))))
                # pp.title('linear phase segments')

                pp.subplot(223)
                pp.cla()

                b,a = sg.iirdesign(wp=0.003,ws=0.0005,gpass=1,gstop=40,ftype='cheby2')

                filt_lin_signal = sg.filtfilt(b,a,lin_signal_w)
                # w, h = sg.freqz(b,a,worN=int(len(filt_lin_signal)/2))

                pp.semilogy(fft.fftfreq(len(lin_signal_w),1/30e3),abs((fft.fft(lin_signal_w))))
                # fft_eval = abs(fft.fftshift(fft.fft(lin_signal)))
                # fft_eval = fft_eval + abs(ny.min(fft_eval))
                # print(fft_eval)
                # pp.plot(fft_eval)
                # pp.semilogy(abs(fft.fftshift(fft.fft(filt_lin_signal))))
                # pp.semilogy(sy.concatenate((abs(h), abs(h))))
                # pp.xlim([len(lin_signal)/2, len(lin_signal)/2 + 1000])
                pp.xlim((0,500))
                pp.title('FFT of linear signal')
                pp.ylabel('Intensity log(arb.)')
                pp.xlabel('Frequency (kHz)')
                # pp.legend()


                pp.subplot(224)
                pp.cla()
                fr, tm, spec = sg.spectrogram(lin_signal_w,
                                              mode='magnitude',
                                              nperseg=1024*5,
                                              noverlap=512*5)
                print(sy.shape(spec))
                # if 'spec_prev' in dir():
                if idx >= do_analyze:
                    # from scipy.ndimage import zoom
                    print(sy.shape(spec))
                    # cspec = sg.convolve2d(spec_prev, spec)
                    # print(sy.shape(cspec))
                    # print('calc dim: ', (len(fr)*2-1, len(tm)*2-1))
                    # print(cspec)
                    # cspec= zoom(cspec,zoom=0.501)
                    # print(sy.shape(cspec))
                    pp.title('Sample mirror z={:0.2f} mm'.format(idx*0.05))
                    pp.xlabel('Acq. time (ms)')
                    pp.ylabel('Frequency (MHz)')
                    # pp.pcolormesh(tm, fr, sy.log10(spec),vmin=4,vmax=15,cmap='CMRmap')
                    print('spec shape: ',sy.shape(spec))
                    cm = pp.imshow(sy.flipud(sy.log10(spec)),
                                   # vmin=2,vmax=5,
                                   cmap='CMRmap',
                                   # extent=[0,trng.max(),0,14],
                                   # aspect=64476/2)
                                   # aspect=2
                                   )
                    pp.ylim((0,0.8))
                    print(cb1)
                    if cb1 is None:
                        cb1 = pp.colorbar(cm, fraction=0.046, pad=0.04)
                        cb1.set_label('Intensity (log$_{10}$ scale)')
                    # pp.ylim((0,0.05))
                    # pp.savefig(self.fname_save+'Lin-s{:02}.png'.format(idx))
                    # while pp.waitforbuttonpress() == False: pass

                spec_prev = spec
                pp.tight_layout()
                pp.pause(0.1)
                self.bpp.wait_for_keys()

            # if do_analyze:
            #     self.analyze_mro_npy(do_plot=True, data= line_signal)

        self.dbglg.info('lin data shape: '+str(sy.shape(lin_fringe_signals)))
        self.lin_fringe_signals_fw = lin_fringe_signals

        self.dbglg.info('save lin data as: '+self.name_lin)
        self.dbglg.info('remove intermediate members forward_data, fitted_phase.    ')
        del self.forward_data
        del self.fitted_phase

        self.store_processing_state(self.name_lin)
        return self

class Filtering(SignalConfig):
    def __init__(self, sigConf):
        self.initSigConf(sigConf)
        self.bpp = BasePlot()


    def find_filter_parameters(self, do_plot = False):
        """
        This should print somewhere an array of the frequency locations by array index values.
        :param do_plot:
        :return:
        """
        # load
        # load data
        self.dbglg.info('load {}'.format(self.name_lin))
        self.restore_processing_state(self.name_lin)

        # will currently only load forward (see inearize_data has reverse scan)
        self.dbglg.warning('load only reverse/forward data set.')
        lin_fringe_signals = self.lin_fringe_signals_fw
        self.dbglg.info('data structure is '+str(sy.shape(lin_fringe_signals)))

        # plot to allow observing the FFT summation
        if do_plot: pp.figure(figsize=(10,7))

        # signal length
        sig_len = len(lin_fringe_signals[0])
        sig_sumAll = None
        sig_fft_sumAll = None

        # create a sum of the fft of all signals to obtain a nice peak trail
        for sig in lin_fringe_signals:
            if not sy.any(sig_sumAll):
                sig_sumAll = sig
                sig_fft_sumAll = abs(fft.fft(sig))[0:int(len(sig)/2)]
            else:
                sig_sumAll += sig
                sig_fft_sumAll += abs(fft.fft(sig))[0:int(len(sig)/2)]

        sig_fft_sumAll_flt = savgol_filter(sig_fft_sumAll, window_length=31, polyorder=6)
        sig_fft_sumAll_pks, _ = find_peaks(sig_fft_sumAll_flt, distance=40, prominence=1.6e7)
        self.dbglg.info('Peak positions (samples): {}'.format(repr(sig_fft_sumAll_pks[0:16])))

        if do_plot:
            pp.semilogy(sig_fft_sumAll)
            pp.semilogy(sig_fft_sumAll_flt)
            pp.semilogy(sig_fft_sumAll_pks, sig_fft_sumAll_flt[sig_fft_sumAll_pks], 'x', ms=10)

            pp.title('FFT sum of all orders')
            pp.xlabel('Sample bins')
            pp.pause(0.1)
            # self.bpp.wait_for_keys()

        # raise Exception('General Stop here.')

        pp.draw()

        pp.tight_layout()
        pp.show(block=False)

        bpp = BasePlot()
        print('press q to quit!')
        bpp.wait_for_keys()
        exit() # it is not useful te proceed with invalid or missing filter center frequencies

    def filter_lin_data(self, do_window={'type':'Gauss'}, do_resample=None, filter_type = None,
                        do_plot=False, plot_start_step=0, zoom=1):
        """
        Filter linearized data.\n
        :param do_window: dict('type': 'Gauss' or 'Tukey')
        :param filter_type: FilterType ojbect
        :param do_plot: If True, do plot the filter results and wait for button press
        :param plot_start_step: start to plot at this b-frame
        :param zoom: for plotting to zoom towards zero frequency in the FFT plot
        :return: return a 3D matrix such that **[orders, b-frames, samples]**
        """

        # todo: 3 The peak detection and frequency calculation on the mirror data is not perfect.
        # However, it is sufficient to obtain repeatable results without manual intervention to allow
        # better comparison of data.

        # todo:2 record spatial correction factores for position and size.
        # see plot_resampled_orders: we do not yet record them independently

        sig_peak_matrix = sy.array([])

        self.dbglg.info('load: {}'.format(self.name_lin))
        # self.restore_processing_state(self.name_lin)
        self.lin_fringe_signals_fw = self.restore_lin_data(self.name_lin)

        # will currently only load forward (see inearize_data has reverse scan)
        self.dbglg.warning('load only reverse/forward data set.')
        lin_fringe_signals = self.lin_fringe_signals_fw
        self.dbglg.info('data structure is ' + str(sy.shape(lin_fringe_signals)))

        self.dbglg.info('Orders to be processed: ' + str(self.max_orders))

        fsigO = [[] for i in range(self.max_orders)]  # prepare array for filtered orders
        # todo:1 For elliptic filter fsigO is one element larger! Why?
        # no problems with latest processings

        self.dbglg.warning('Size of fsigO: ' + str(sy.shape(fsigO)))
        # warnings.warn('For elliptic filter fsigO is one element larger! Why?')

        n_pos = range(len(lin_fringe_signals))


        # signal window before filtering
        # this goes perhaps easier with a rect window and multiplying the sides with gaussian
        # todo:2 The window can be improved by fitting a partial gaussian to one side of the emerging signal and
        # generating a symmetrically shaped input signal, in which case the perfect gaussian shape should always
        # generate a well cut-off frequency spectrum.
        # Further, for each filter bandwidth a different window can be generated to match best to the expected signal
        # shape.

        slen = sy.shape(lin_fringe_signals)[1]

        if 'Gauss' in do_window['type']:
            # nearly square window
            # pass_width = 3000
            # stop_width = 6
            # flat gauss
            # pass_width = 10
            # stop_width = 4
            pass_width = 10
            stop_width = 5
            self.td_window = self.gauss_window(slen, stop_width, pass_width, flat=True)
        elif 'Tukey' in do_window['type']:
            self.td_window = self.tukey_window(slen,
                                          stop_width = do_window['stop_width'],
                                          pass_width = None,
                                          alpha = do_window['alpha'],
                                          sym=do_window['sym'])

        self.dbglg.warn('Window is: ' + do_window['type'])
        self.dbglg.info('Keep self.td_window in memory!')


        if not 'gauss' == self.filter_type.name:
            filters = self.build_filter(slen = slen, filter_type = self.filter_type)

        bpp = BasePlot()

        if do_resample is not None:
            new_sample_len = do_resample['new_sample_rate']
            self.dbglg.info('Do resampling of data to '+ str(new_sample_len))

        for lsig, pos in zip(lin_fringe_signals, n_pos):
            if pos%10 == 0:
                self.dbglg.info('time at '+str(pos))
            if do_plot and pos > plot_start_step:
                self.dbglg.info('filter: ' + str(pos) + ' pos (mm): ' + str(pos * 5e-3))

            # For a scan rate of 152.6 Hz the amount of samples are much larger than at higher scan frequencies.
            # Consequently filtering for lower scan frequencies appears to take longer and downsampling beforehand
            # may speed up processing by equal quality
            if do_resample is not None:
                O = 1  # select base sample length from this order (first order O=1 default)
                dslen = do_resample['new_sample_rate'] * O  # a sample rate suitable for filtering keeping the best SNR
                dt = self.sample_interval * slen / dslen
                slen = dslen
                srng = sy.arange(0, slen)
                trng = sy.linspace(0, dt * slen, slen)
                lsig = sg.resample(lsig, slen)
                self.td_window = sg.resample(self.td_window, slen)
            else:
                # original signal full sample rate
                dt = self.sample_interval
                slen = len(lsig)
                dslen = slen
                srng = sy.arange(0, slen)  # signal sample range
                trng = srng * dt  # signal time range

            #
            # fftbaseline = self.apply_boxcar_fft(lsig,order=O)

            # self.dbglg.info('signal shape: '+str(sy.shape(lsig)))
            # self.dbglg.info('window shape: '+str(sy.shape(gauss_win)))

            lsig = lsig * self.td_window

            msigFFT = sy.absolute(fft.fftshift(fft.fft(lsig)))
            htime = 1/2  # half the sample time due to fft reduction of samples.
            frng = fft.fftshift(fft.fftfreq(n=dslen, d=dt)) * htime
            # This not a reduction in the time domain! But as we cannot increase the bins (n) we need to half the time
            # in the frequency domain.

            # limit range of FFT to one side
            msigFFTss = msigFFT[int(slen / 2):]
            frngss = frng[int(slen / 2):]

            # peak detection discussion
            # https://blog.ytotech.com/2015/11/01/findpeaks-in-python/
            # general discussion and point to an external library.
            # https://gist.github.com/endolith/250860
            # copy from ML. This is a very simple and limited algorithm but may be just about right for this purpose.
            # Problems occur if noise content is large.
            # A more enhanced discussion at https://gist.github.com/sixtenbe/1178136
            # https://pypi.python.org/pypi/PeakUtils
            # this package is easy to understand in source code and provides different methods of fitting and baseline
            # detection which may be helpful for our own purpose.
            # pip3 install peakutils 1.0.3

            # It may be of interest to investigate if it is perhaps better to extract the signals at different
            # sample rates.
            # I.e. for the first order we can work on a downsampled signal, yet keeping the original signal.
            # For higher orders we switch then step by step to higher sampling rates.

            # 1 find first peak
            # 2 find second peak
            # 3 filter first peak with second peak frequ.
            # 4 monitor array with second peak and find third peak.
            # repeat 3

            # todo: this peak stuff appears to be not in use
            mean_level = msigFFTss.mean()  # baseline for peak
            peak_width_smp = sy.where(msigFFTss > mean_level)  # all values above baseline
            sig_peaks = detect_peaks.detect_peaks(msigFFT, threshold=1e5, mpd=15)
            sig_peak_matrix = sy.hstack( (sig_peak_matrix, sy.array(sig_peaks)))
            # print('peaks: '+str(sig_peak_matrix))

            if 'gauss' == filter_type.name:
                self.gauss_filter(lsig, slen, dslen, fsigO, self.td_window,
                                  pos, msigFFT, trng, frng, do_plot, plot_start_step, zoom, filter_type)

            elif 'ellip' in filter_type.name or 'cheby2' in filter_type.name:
                self.do_filter(do_plot, frng, fsigO, self.td_window, lsig, msigFFT, pos,
                               slen, plot_start_step, trng, filters = filters, filter_type = self.filter_type)
            else:
                self.dbglg.info('Filter type: ' + filter_type.name + ' not defined.')
                break

        # store filtered signals as class member
        self.dbglg.info('Filtering with ' + filter_type.print_params() + ' finished.')
        self.dbglg.info('shape of fsigO ' + str(sy.shape(fsigO)))
        self.filtered_data_fw = fsigO
        self.dbglg.info('Keep self.filtered_data_fw in memory!')

        del (lin_fringe_signals)
        del (fsigO)
        del (self.lin_fringe_signals_fw)

        self.dbglg.info('store {}'.format(str(self.__class__) + self.name_flt))
        self.store_processing_state(self.name_flt)
        return self

    def calc_envelope(self,data):
        """
        Attempt for parallel processing.
        Out of some reasons this needs to be in a function.
        '''Must be defined as class member to be able to be pickled during MP!'''
        :param data:
        :return:
        """
        # print(default_timer())
        # print('calc_envelope',sy.shape(data))

        # return abs(sg.hilbert(data, axis=2))
        return abs(sg.hilbert(data))

    def numba_envelope(self, envelope_jit):
        self.envelope_jit = envelope_jit

    def hilbert_data(self, instance, use_mp = True):
        '''
        :param instance: The parameter is currently required for the multiprocess calls.
         The multiprocess call can only work if it knows the actual object instance to copy to a new
         core.
         This also points to a somewhat overhead to create a parallel process and to be careful that
         the object does not contain a variable holding all or large junks of data.
        :param delete_data: If True delete the intermediate data (not used yet)
        :return: No return value is provided. All processed data are stored in files.
        '''

        self.dbglg.info('load {}'.format(self.name_flt))
        self.restore_processing_state(self.name_flt)
        assert 'filtered_data_fw' in dir(self), '=====>  Please call filter_lin_data! '
        fds = self.filtered_data_fw
        self.dbglg.info('hilbert data shape: '+str(sy.shape(fds)))

        fdsh = []
        if use_mp:
            cpu_count = mp.cpu_count() - 2 # keep two cores available
            # self.dbglg.info('load pools for '+str(cpu_count)+' cores .')
            with mp.Pool(processes = (cpu_count)) as p:
                fdsh = (p.map(instance.calc_envelope, fds))
        else:
            # self.dbglg.info('MP disabled. Process hilbert data sequentially.')
            for fd in fds:
                fdsh.append( self.calc_envelope(fd)) #/self.td_window)
                # fdsh.append( abs(self.envelope_jit(fd)) )
                fd = []

        # debug start
        # for o in range(12):
        #     for i in range(350):
        #         pp.cla()
        #         pp.plot(fdsh[o,i])
        #         pp.draw()
        #         self.pause()
        # debug stop

        self.hilbert_data_fw = fdsh

        self.dbglg.info('store {}'.format(self.name_hlb))
        del(fds) # not sure if we need to call this but for now it does not complain.
        del(fdsh)
        del(self.filtered_data_fw)

        self.store_processing_state(self.name_hlb)
        return self

    def build_filter(self, slen, filter_type):
        # Some more considerations to the selection of the filter type
        # http://www.martinos.org/mne/dev/auto_tutorials/plot_background_filtering.html
        # https://mynameismjp.wordpress.com/2012/10/15/signal-processing-primer/
        iir_sos_filters = {}
        for peak in self.peak_list:
            CF = peak
            # print('CF (Hz) ',CF/slen*max(frng)*2,' (smp): ', CF)
            CF = CF / slen * 7.8
            # print('CF (norm)', CF)
            # dwp = 0.0026
            # dws = 0.0022
            # dwp = self.dwp
            # dws = self.dws
            sb1 = sb2 = filter_type.sb
            pb1 = pb2 = filter_type.pb
            WP = [CF - pb1, CF + pb2]
            WS = [CF - sb1, CF + sb2]
            name = filter_type.name
            self.dbglg.info(name + ' params: SB {:1.7f} {:1.7f} {:1.7f} {:1.7f}'.format(CF-sb1,CF-pb1,CF+pb2,CF+sb2))
            # print('WP', WP)
            # print('WS', WS)
            # use iirdesign to obtain a more stable filter for extreme conditions.
            # https://www.dsprelated.com/showarticle/194.php
            # very nice comparison
            # https://www.dsprelated.com/showarticle/164.php
            # b,a = sg.iirdesign(wp=[0.02,0.025],ws=[0.0001,0.040],gpass=0.1,gstop=60,ftype='cheby2')
            # b,a = sg.iirdesign(wp=[0.2, 0.3],ws=[0.1,0.5],gpass=5,gstop=60,ftype='ellip',output='ba')

            # todo: -1 for a set of different filter parameters the sosdata remain the same
            # original processing
            # *******************
            # sosdata = sg.iirdesign(wp=WP, ws=WS, gpass=0.1, gstop=40, ftype='ellip', analog=False, output='sos')
            # print(sosdata)
            # zi = sg.sosfilt_zi(sosdata)
            # iir_sos_filters[peak] = (sosdata, zi)

            # based on
            # https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.zpk2sos.html
            # some more info here http://dsp.stackexchange.com/questions/1966/how-can-i-break-a-filter-down-into-second-order-sections
            #
            z,p,k = sg.iirdesign(wp=WP, ws=WS, gpass=0.001, gstop=120, ftype=name, analog=False, output='zpk')
            # print(z,p,k)
            sosdata = sg.zpk2sos(z,p,k,pairing='keep_odd')
            zi = sg.sosfilt_zi(sosdata)
            # print(sosdata,'\n', zi)
            iir_sos_filters[peak] = (sosdata, zi)

        return iir_sos_filters

    def do_filter(self, do_plotSteps, frng, fsigO, window,
                  lsig, msigFFT, pos, slen, start_do_plotSteps, trng, filters, filter_type):
        # todo:2 check the parameter signature of ellip_filter and if we can simplify

        # some filter design using ordinary filter builder.
        # The problem is however, that we want to filter with the full amount of samples and the actual bandwidth
        # for the filter parameters become very small relative to the full bandwidth, and the filter becomes unstable
        # in most of the cases.
        # So it is better to use an second-order section (SOS) filter
        filter_responses = []
        plt_filt_signals = []
        on = 0
        for peak in self.peak_list:
            sosdata, zi = filters[peak]
            # apply filter
            flsig, yzi = sg.sosfilt(sosdata, lsig, zi=zi)
            # store signal for step plotting
            plt_filt_signals.append(flsig)

            # store filtered signal
            # print('order n:',on)
            fsigO[on].append(flsig)
            on = on + 1  # goto next order

            # b,a = sg.sos2tf(sosdata)
            # w, h = sg.freqz(b,a,worN=int(slen))

            # sosfreqz is not yet available, but some preliminary implementation is available from
            # https://github.com/scipy/scipy/blob/master/scipy/signal/filter_design.py
            worN = len(frng)
            h = 1.
            for row in sosdata:
                w, rowh = sg.freqz(row[:3], row[3:], worN=worN, whole=True)
                h *= rowh

            # to fit the array into a full length double sided fft we pad zeros to the left
            # h = sy.concatenate((sy.zeros(len(frng)), h))
            # store filter responses for plotting
            filter_responses.append(h)

            if on >= self.max_orders: break

        def plot_gauss_win():
            pp.cla()
            ms = 1000
            pp.plot(trng * ms, (lsig)/lsig.max())
            pp.plot(trng * ms, window )
            pp.grid(True)
            # pp.ylim([-40000, 40000])
            pp.xlabel('sample time (ms)')
            pp.ylabel('Amplitude (arb.)')
            pp.title('Interference signal of\nsingle reflection.')

        kHz = 1 / 1000

        def plot_filter_response():
            # pp.hold(do_hold)
            pp.cla()
            for fr in filter_responses:
                pp.semilogy(frng * kHz,  abs(fft.fftshift(fr)) * max(msigFFT))
            pp.semilogy(frng * kHz, msigFFT) #plot last to have synch colors

            pp.xlim([0, max(frng * kHz * self.max_khz) ])
            pp.ylim([1, 1e9])
            pp.xlabel('sample frequency (kHz)')
            pp.ylabel('Amplitute (arb.)')
            # pp.title('linearized FFT')
            pp.title('Filter response and\nsample signal.')
            pp.grid(True)
            # pp.cla()

        def plot_save_single_plots():

            whatText = self.path+ '/'+filter_type.print_params()+'_1st_pos{:02g}'.format(start_do_plotSteps)
            pp.figure(1,figsize=(11,7))
            plot_gauss_win()
            # pp.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
            pp.tight_layout()
            pp.draw()
            pp.pause(0.1)
            pp.savefig(whatText + '_signal.png')
            pp.savefig(whatText + '_signal.pdf')

            pp.figure(2,figsize=(11,7))
            plot_filter_response()
            pp.xlim([25,40])
            pp.tight_layout()
            pp.draw()
            pp.pause(0.1)

            pp.savefig(whatText + '.png')
            pp.savefig(whatText + '.pdf')

            pp.figure(3,figsize=(11,7))
            plot_filter_response()
            # pp.xlim([25,40])
            pp.tight_layout()
            pp.draw()
            pp.pause(0.1)
            pp.savefig(whatText + '_full.png')
            pp.savefig(whatText + '_full.pdf')

            pp.rcParams['font.size']=14
            pp.figure('subplots')

        # if pos == start_do_plotSteps:
        #     plot_save_single_plots()

        if do_plotSteps and pos > start_do_plotSteps:
            pp.figure(num='filter response',tight_layout=True,figsize=(10,7))
            def do_not_plot():
                pass
            # pp.subplot(221)
            # plot_gauss_win()
                # By observation of the timedependent plot the first order has about 19 cycles within a time of about 2 ms.
                # The frequency calculated is then 19/2e-3 = 9500 Hz (9.5 kHz)

            pp.subplot(211)
            # make new range corresponding to our expected data -> 60 kHz 1st order
            # The values originally are skewed due to resampling or other.
            frng = sy.linspace(-3.8e6, 3.8e6, len(msigFFT))
            plot_filter_response()

            def do_not_plot():
                pass
                pp.subplot(223)
                pp.cla()
                ms = 1000  # make values to milli seconds
                # each fsigO contains now max amount orders for one position here, as we still process each position now,
                # we do access the current processing step with cnt!

                for fsigN in fsigO[0:5]:
                    sigO = fsigN[pos]
                    pp.semilogy(abs(sg.hilbert(sigO)))
                pp.ylim((1, 1e5))
                pp.xlabel('sample time (ms)')
                pp.ylabel('Amplitude (arb.)')
                pp.grid(True)
                pp.title('Envelope of filtered signals')

            pp.subplot(212)

            pp.cla()
            for fsigN in fsigO:
                sigO = fsigN[pos]
                fft_filt_sig = abs(fft.fftshift(fft.fft(sigO)))
                pp.semilogy(frng * kHz, fft_filt_sig)

            pp.xlim([0, max(frng * kHz * self.max_khz) ])
            pp.xlabel('sample frequency (kHz)')
            pp.ylabel('Amplitude (arb.)')
            pp.title('FFT of signals')
            pp.grid(True)

            pp.pause(0.1)
            # while pp.waitforbuttonpress() == False: pass
            self.bpp.wait_for_keys()


    def gauss_filter(self, lsig, slen, dslen, fsigO, td_window, pos, msigFFT,
                     trng, frng, do_plot_steps, start_do_plotSteps, zoom, filter_type):

        # We do not know if this would work.
        if slen != dslen:
            self.dbglg.warn('Gaussian filter for downsampled data! Double check if this can work.')

        w = filter_type.gbw
        gaussws = []  # gaussian windows
        fftSigO = []  # fft of windowed order
        on = 0  # order index value, i.e. 0...11 are 12 orders
        # peak_list = (63, 122, 184, 246, 307, 369, 426, 494, 554, 616, 678, 742, 805)

        assert len(self.peak_list) >= self.max_orders, 'You may need to define currently more frequencies manually.'

        lsig_max = lsig.max()
        gauss_max = td_window.max()
        def plot_gauss_win():
            pp.cla()
            ms = 1000
            pp.plot(trng * ms, lsig/lsig_max)
            pp.plot(trng * ms, td_window / gauss_max)
            pp.grid(True)
            # pp.ylim([-40000, 40000])
            pp.xlabel('sample time (ms)')
            pp.ylabel('Amplitude (arb. units)')
            pp.title('Interference signal of single reflection.')

        kHz = 1 / 1000

        def plot_filter_response( filter_responses):
            # pp.hold(do_hold)
            pp.cla()
            pp.hold(True)
            pp.semilogy(frng * kHz, msigFFT)
            for fr in filter_responses:
                pp.semilogy(frng * kHz, fr/ny.max(fr)*ny.max(msigFFT))

            pp.xlim([0, max(frng * kHz * self.max_khz)])
            pp.ylim([1, 1e9])
            pp.xlabel('sample frequency (kHz)')
            pp.ylabel('Amplitute (arb. units)')
            # pp.title('linearized FFT')
            pp.title('Filter response and sample signal.')
            pp.grid(True)
            pp.hold(False)
            # pp.cla()

        def plot_save_single_plots( filter_responses):
            whatText = self.path + '/'+filter_type.print_params()+'_1st_pos{:02g}'.format(start_do_plotSteps)
            pp.rcParams['font.size'] = 24
            pp.figure(1, figsize=(11, 7))
            plot_gauss_win()
            # pp.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
            pp.tight_layout()
            pp.draw()
            pp.pause(0.1)
            pp.savefig(whatText + '_signal.png')
            pp.savefig(whatText + '_signal.pdf')

            pp.figure(2, figsize=(11, 7))
            plot_filter_response( filter_responses )
            pp.xlim([25, 40])
            pp.tight_layout()
            pp.draw()
            pp.pause(0.1)
            pp.savefig(whatText + '.png')
            pp.savefig(whatText + '.pdf')

            pp.figure(3, figsize=(11, 7))
            plot_filter_response( filter_responses )
            # pp.xlim([25,40])
            pp.tight_layout()
            pp.draw()
            pp.pause(0.1)
            pp.savefig(whatText + '_full.png')
            pp.savefig(whatText + '_full.pdf')

            pp.rcParams['font.size'] = 14
            pp.figure('subplots')

            self.bpp.show()

        try:
            if not self.peak_list_logged: pass
        except AttributeError as ne:
            self.dbglg.info('Filter peak_list: ' + str(self.peak_list))
            self.peak_list_logged = True


        def do_filter(on):
            for f in self.peak_list[0:self.max_orders]:

                def create_gauss_window(slen, dslen, w, f):
                    # Create both sides of the FFT signal.
                    left_part = sy.exp(-((sy.arange(0, slen) - dslen / 2 - f) / w) ** 2)
                    right_part = sy.exp(-((sy.arange(0, slen) - dslen / 2 + f) / w) ** 2)
                    # right_part = right_part[]
                    fft_gaussw = left_part + right_part
                    return fft_gaussw

                sw = 0.1
                pw = 0.01
                # gaussw = self.gauss_window(slen, sw, pw, flat=True)
                fft_gaussw = create_gauss_window(slen, dslen, w, f)
                gaussws.append(fft_gaussw) # for showing them

                FFTSig = fft.fftshift(fft.fft(lsig))

                def low_pass_filter(signal):
                    z, p, k = sg.iirdesign(wp=0.4, ws=0.49, gpass=0.1, gstop=40, ftype='ellip', analog=False, output='zpk')
                    # print(z,p,k)
                    sosdata = sg.zpk2sos(z, p, k, pairing='keep_odd')
                    zi = sg.sosfilt_zi(sosdata)
                    # print(sosdata,'\n', zi)
                    # apply filter
                    flsig, yzi = sg.sosfilt(sosdata, signal, zi=zi)
                    return flsig

                def reshape( fftSig, fft_gaussw ):
                    # This not yet strictly reshaping!
                    # Because we actually just replace the real-part of the signal with fitted data.
                    # prepare reshaping with Gaussian window
                    # fftSigAng = sy.unwrap(sy.angle(FFTSig))
                    # try to show that removing high frequency noise of the phase can demonstrate improved image quality.
                    # fftSigAngFlt = low_pass_filter(fftSigAng)
                    # apply fft_gaussw to find the right magnitute for the frequency
                    # rs_fft_gaussw = create_gauss_window(slen,dslen, w, f)
                    # rs_fft_gaussw = fft_gaussw
                    # fftMagnitute = max(abs(FFTSig) * rs_fft_gaussw)
                    # rs_fft_gaussw *= fftMagnitute

                    # reshape with the Gauss window, meaning we just use the Gauss window as the signal.
                    # rsFFTSig = list(map(cmath.rect, rs_fft_gaussw, fftSigAng))
                    # rsFFTSig = sy.array([cmath.rect(s,a) for s,a in zip(fft_gaussw, fftSigAng)])
                    rsFFTSig = fftSig * fft_gaussw
                    return rsFFTSig

                FFTSig = reshape( FFTSig, fft_gaussw )

                # FFTSig = FFTSig * fft_gaussw

                def plot_gauss_template():
                    pp.subplot(221)
                    pp.title('Input signal')
                    pp.plot(lsig)
                    pp.grid(True)
                    pp.subplot(222)
                    pp.title('Raw')
                    pp.plot(abs(FFTSig), label='sig')
                    normal = FFTSig[int(dslen / 2 + f - 10):int(dslen / 2 + f + 10)].max()
                    pp.plot(fft_gaussw * normal, label='gauss')
                    pp.legend()
                    pp.xlim([4000, 6000])
                    pp.grid(True)
                    pp.subplot(223)
                    pp.plot(abs(fft_gaussw*normal))
                    pp.xlim([4000, 6000])
                    pp.grid(True)
                    pp.title('Gauss applied')
                    pp.subplot(224)
                    pp.plot(fft.fftshift(fft.fft(fft.fftshift(fft_gaussw * normal ))))
                    pp.grid(True)
                    pp.title('Resulting TD signal')
                    pp.pause(0.1)
                    self.bpp.wait_for_keys()
                # plot_gauss_template()

                fftSigO.append(FFTSig)

                # though the signal is real minute errors will end up in the imag part.
                fsigO[on].append(ny.real((fft.ifft(fft.fftshift(FFTSig)))))
                on = on + 1
            return fsigO

        # This does compile and does also calculate but it slows down the whole function.
        # do_filter_jit = numba.jit('double[:,:](double)')(do_filter)
        fsigO = do_filter(on)

        # if do_plot_steps:
        #     if pos == start_do_plotSteps:
        #         plot_save_single_plots( filter_responses = gaussws)

            #todo: -1 the plotting framework must be removed to an external method.

        if do_plot_steps and pos >= start_do_plotSteps:
            fig = pp.figure('Filter response', figsize=(11,7))
            pp.subplot(221)
            ms = 1000
            pp.cla()
            pp.plot(trng * ms, lsig/lsig_max)
            pp.plot(trng * ms, td_window / gauss_max)
            pp.grid(True)
            # pp.ylim([-40000, 40000])
            pp.xlabel('sample time (ms)')
            pp.ylabel('Amplitude (samples)')
            pp.title('linearized')


            pp.subplot(222)
            pp.cla()
            kHz = 1 / 1000

            plot_in_frequency = True

            if plot_in_frequency:
                # make new range corresponding to our expected data -> 60 kHz 1st order
                # The values originally are skewed due to resampling or other.
                frng = sy.linspace(-3.8e6,3.8e6,len(msigFFT))
                for fftSig, gauss in zip(fftSigO, gaussws):
                    pp.semilogy(frng * kHz, (gauss) * 10e6 + 1e0 )
                pp.semilogy(frng * kHz, msigFFT)
                pp.xlim([0, max(frng) * kHz * self.max_khz])
                pp.xlabel('sample frequency (kHz)')
            else:
                # pp.figure('Filter')
                pp.semilogy( msigFFT)
                for fftSig, gauss in zip(fftSigO, gaussws):
                    pp.semilogy( (gauss) * 10e6 + 1e0)
                pp.xlim([int(len(fftSigO)/2.0), int(len(fftSigO[0])*zoom)])
                pp.xlabel('sample frequency (samples)')
                # pp.show(block=False)
                # self.bpp.wait_for_keys()

            pp.ylim([1, 1e9])
            pp.ylabel('Amplitude (sample power)')
            pp.title('linearized FFT at z={:0.2f} mm'.format(pos*0.05))
            pp.grid(True)


            with open(self.path+'/freq_response_p{:03d}.pkl'.format(pos),'wb+') as fid:
                pickle.dump({'frng':frng,'msigFFT':msigFFT, 'gaussws':gaussws},fid )

            pp.subplot(223)
            pp.cla()
            ms = 1000 # make values to milli seconds

            # each fsigO contains now 12 orders for one position here, as we still process each position now,
            # we do access the current processing step with cnt!
            self.dbglg.info('Signal / Mirror step position is: ' + str(pos) + '(' + str(pos * self.mirror_step_size) + ') m')
            for fsigN in fsigO:
                sigO = fsigN[pos]
                pp.semilogy(trng * ms, abs(sg.hilbert((sigO))) / td_window)
                # pp.plot(trng * ms, sigO)

            pp.ylim([-20000, 30000])
            pp.xlabel('sample time (ms)')
            pp.ylabel('Amplitude (samples)')
            pp.grid(True)
            pp.title('Envelope of filtered signals ')

            pp.subplot(224)
            pp.cla()
            for fsigN in fsigO:
                sigO = fsigN[:][pos-1]
                pp.semilogy(frng * kHz, abs(fft.fftshift(fft.fft(sigO))))
            pp.xlim([0, max(frng * kHz) * self.max_khz])
            pp.ylim((1e-15,1e11))
            pp.xlabel('sample frequency (kHz)')
            pp.ylabel('Amplitude (sample power)')
            pp.title('FFT of filtered signals')
            pp.grid(True)

            pp.tight_layout()
            # if not pp.fignum_exists('Filter response'):
            pp.draw()
            # pp.savefig('filterSignalResponse_pos{}.pdf'.format(pos))
            pp.show(block=False)
            self.bpp.wait_for_keys()


    def ellip_filter_simple(self, sig, params = None, do_hold = False, do_plotSteps = False, log=False ):
        if log:
            self.dbglg.info('Filter sig: '+str(sy.shape(sig)))
        # some filter design using ordinary filter builder.
        # The problem is however, that we want to filter with the full amount of samples and the actual bandwidth
        # for the filter parameters become very small relative to the full bandwidth, and the filter becomes unstable
        # in most of the cases.
        # So it is better to use an second-order section (SOS) filter

        slen = len(sig)

        if not params:
            dwp = self.dwp
            dws = self.dws
            peak_list = self.peak_list
        else:
            dwp = params['dwp']
            dws = params['dws']
            peak_list = params['peak_list']

        for peak in self.peak_list:
            CF = peak
            CF = CF / slen * 2
            to_freq = 1/2/self.sample_interval
            if log:
                self.dbglg.info('CF: '+str( CF) + ' ' + str( CF*to_freq))
                self.dbglg.info('dwp: +/-'+str( self.dwp_Hz) + ' dws: ' + str( self.dws_Hz))

            WP = [CF - dwp, CF + dwp]
            symmetrize = 1.2
            WS = [CF - dwp - dws, CF + dwp + dws / symmetrize]
            if log:
                self.dbglg.info('WP WS: '+ str( list(map(lambda ws: ws*to_freq,WP))) + '; ' +  str([ws*to_freq for ws in WS]))
            # use iirdesign to obtain a more stable filter for extreme conditions.
            # https://www.dsprelated.com/showarticle/194.php
            # very nice comparison
            # https://www.dsprelated.com/showarticle/164.php
            # b,a = sg.iirdesign(wp=[0.02,0.025],ws=[0.0001,0.040],gpass=0.1,gstop=60,ftype='cheby2')
            # b,a = sg.iirdesign(wp=[0.2, 0.3],ws=[0.1,0.5],gpass=5,gstop=60,ftype='ellip',output='ba')
            sosdata = sg.iirdesign(wp=WP, ws=WS, gpass=0.1, gstop=60, ftype='ellip', output='sos')
            zi = sg.sosfilt_zi(sosdata)

            # apply filter
            fsig, yzi = sg.sosfilt(sosdata, sig, zi=zi)

            # b,a = sg.sos2tf(sosdata)
            # w, h = sg.freqz(b,a,worN=int(slen))

            # sosfreqz is not yet available, but some preliminary implementation is available from
            # https://github.com/scipy/scipy/blob/master/scipy/signal/filter_design.py
            worN = slen
            h = 1.
            for row in sosdata:
                w, rowh = sg.freqz(row[:3], row[3:], worN=worN, whole=True)
                h *= rowh

        return fsig, h

    def tukey_window(self, slen, stop_width = None, pass_width = None, alpha = None, sym = None):
        self.dbglg.warn('Tukey window stop_width: {}'.format(stop_width))
        self.dbglg.warn('Tukey window ignore currently pass_width: {}'.format(pass_width))

        zero_pad_left = zero_pad_right = []
        if not stop_width:
            window_width = slen
        else:
            # reduce the window length to cut-off some of the signal
            window_width = slen - 2*stop_width
            zero_pad_left = stop_width
            zero_pad_right = zero_pad_left
            if zero_pad_left + zero_pad_right + window_width < slen:
                zero_pad_right += 1
            zero_pad_left = sy.zeros(zero_pad_left)
            zero_pad_right = sy.zeros(zero_pad_right)


        if alpha == None and sym == None:
            return sy.concatenate((zero_pad_left, sg.tukey(window_width), zero_pad_right))
        elif alpha == None :
            return sy.concatenate((zero_pad_left, sg.tukey(window_width, sym=sym), zero_pad_right))
        elif sym == None:
            return sy.concatenate((zero_pad_left, sg.tukey(window_width, alpha=alpha), zero_pad_right))
        else:
            return sy.concatenate((zero_pad_left, sg.tukey(window_width, alpha=alpha, sym=sym), zero_pad_right))




    def gauss_window(self, slen, stop_width, pass_width, flat=True):
        '''
        It may appear that an         sy.signal.tukey() window could provide an better way to avoid artefacts.
        Generate a window with Gaussian side lobes to limit the signal range at the extreme ends,
        to avoid filter artefacts.
        :return:
        '''
        # prepare window base with according signal length
        base_window = sy.ones(slen)
        # import scipy.signal.windows as windows
        # pp.plot(windows.get_window(('gauss',10),100))
        # pp.grid()
        # pp.show()
        if flat:
            # create a Gaussian for the beginning of the window edge
            xr = sy.arange(0, slen)
            x1 = int(slen / stop_width)  # location
            w1 = int(slen / pass_width)  # width
            g1 = 1.0  # amplitude should remain 1 corresponding to the base_window
            gauss_win_1 = g1 * sy.exp(-((xr - x1) / w1) ** 2)

            # set all values to 1 after the Gaussian part has reached 1
            [gmax], = sy.where(ny.max(gauss_win_1) == gauss_win_1)
            gauss_win_1[gmax:-1] = 1

            # get the left side of the Gaussian part
            # gauss_win_1 = gauss_win_1[0:sy.where(gauss_win_1 == max(gauss_win_1))[0][0]]
            #
            x2 = int(slen - slen / stop_width)
            w2 = int(slen / pass_width)
            g2 = g1
            gauss_win_2 = g2 * sy.exp(-((xr - x2) / w2) ** 2)
            [gmax2], = sy.where(ny.max(gauss_win_2) == gauss_win_2)
            gauss_win_2[0:gmax2] = 1
            # all other regions have value 1 and do not change
            # consequently by multiplication we modify only regions that are not equal 1.0
            gauss_flat_top = base_window * gauss_win_1 * gauss_win_2
            return gauss_flat_top
        else:
            xr = sy.arange(0, slen)
            xc = slen / 2
            w = pass_width
            g1 = 2.0

            gauss_window = g1 * sy.exp( -((xr - xc) / w)**2)
            return gauss_window


    def plot_filtered_data(self,from_order = 0):
        '''Animation like sweep through all different orders.
        order n=0 is the 1st order!'''
        fds = self.filtered_signals_per_Order
        stepsize = 5 # um
        step_rng = range(0,len(fds)*stepsize,stepsize)
        order_rng = range(from_order,self.max_orders)
        ylim = [1e-1,1e5]
        evtpar = {'do_jmp':False, 'ylim':ylim}

        def key_pressed(e,par):
            print(e.key)
            if e.key == 'n':
                par['do_jmp'] = True
            if e.key == 'up':
                par['ylim'] = [par.get('ylim')[0]*2,par.get('ylim')[1]*2]
            if e.key == 'down':
                par['ylim'] = [par.get('ylim')[0]/2,par.get('ylim')[1]/2]
        pp.figure('Filtered data')
        pp.ion()
        pp.hold(False)
        pp.gcf().canvas.mpl_connect('key_press_event', lambda evt: key_pressed(evt,evtpar))

        for o,order in zip(fds[from_order:], order_rng):
            for s in o:
                pp.semilogy(sg.resample(abs(sg.hilbert(s)),100),'.')
                pp.ylim(evtpar['ylim'])
                pp.title('Signal at pos {:g} (um) of order {:g}'.format(0,order+1))
                pp.draw()
                pp.pause(0.2)

                if evtpar['do_jmp']:
                    print('jump')
                    evtpar['do_jmp'] = False
                    break



class SignalAnalysis(SignalConfig):
    def __init__(self, sigConf):
        self.initSigConf(sigConf)

    def sensitivity_CI(self, filter_type='ellip', do_plot_steps=False, plot_step_start=0, zoom=1):
        '''start_to_plotSteps = 0 ... max mirror steps (i.e. 200)'''

        fname = self.path  + self.fname_save + '_fr' +'.pkl'
        self.dbglg.info('load: '+fname)

        with open(fname, 'rb+') as fid:
            data = pickle.load(fid)

        fringe_signal = sy.mean(data['forward'],axis=0)

        # fringe_signal = sg.resample(fringe_signal, 10000)
        # will currently only load forward (see inearize_data has reverse scan)
        self.dbglg.warning('load only reverse/forward data set.')
        self.dbglg.info('data structure is ' + str(sy.shape(fringe_signal)))

        print(sy.shape(fringe_signal))

        if 'gauss' in filter_type:
            self.dbglg.warning('Gauss simple not implemented. Exit()')
            exit()
        elif 'ellip' in filter_type:
            fsignal, h = self.ellip_filter_simple( fringe_signal)
        orig_signal = fringe_signal
        fringe_signal = fsignal

        slen = len(fringe_signal)
        show_rng = sy.arange(int(slen/2),int(slen/2)+ 300)  # signal sample range

        ms = 1000
        trng = sy.arange(0,slen) / self.sample_rate * ms

        frng = sy.arange(-1,1,2/slen) /self.sample_interval/2 # 1/2 due to halve of samples in FFT


        pk_idx = sy.argmax(sy.sqrt(fringe_signal[20000:-1]**2))+20000
        pk_rng = range(pk_idx-2, pk_idx+2)
        peak = sy.sqrt(sy.mean(fringe_signal[pk_rng]**2))
        self.dbglg.info('peak: '+str( peak)+' at '+str(pk_idx))

        # if the range for the noise is selected too small then an error of up to 2 dB is too expect.
        noiseRMSbeg = pk_idx - 25000
        noiseRMSend = pk_idx - 18000
        noiseRMStbeg = noiseRMSbeg/self.sample_rate * ms
        noiseRMStend = noiseRMSend/self.sample_rate * ms
        noiseRMS_rng = range( noiseRMSbeg, noiseRMSend)
        noiseRMS = sy.sqrt(sy.mean(sy.array(fringe_signal[noiseRMS_rng])**2))
        self.dbglg.info('noise RMS: ' + str( noiseRMS))

        snrr = peak/noiseRMS
        snrdb = 20*sy.log10(peak/noiseRMS)
        self.dbglg.info('SNR: '+str(snrr) + ' dB_20 '+ str( snrdb ))

        rms_sig = sy.sqrt(fringe_signal**2)
        params = {}
        params['peak_list'] = [5]
        params['dwp'] = 0.0045
        params['dws'] = 0.0005
        # rms_fsig, _ = self.ellip_filter_simple(rms_sig, params)
        rms_fsig = abs(sg.hilbert(fringe_signal))
        p1 = BasePlot()

        pp.subplot(221)
        pp.plot(orig_signal)
        pp.plot(fsignal)
        pp.plot((noiseRMSbeg, noiseRMSbeg), (-peak,peak) )
        pp.plot((noiseRMSend, noiseRMSend), (-peak,peak) )
        pp.grid()
        # pp.title(self.fname + ' \n' + '{:02.3g} dB'.format(snrdb))

        angValIdx = self.fname.rfind('refang')+len('refang')
        angVal = int(self.fname[angValIdx:].strip('\.npy'))
        ref_attn = angVal * 2.0 / 360
        self.dbglg.info('angle: '+str(angVal)+' attn: '+str(ref_attn))

        amnt_digits = 5
        feedbackPowIdx = self.fname.rfind('nW_') - amnt_digits
        feedbackPow = self.fname[feedbackPowIdx:feedbackPowIdx+amnt_digits]

        pp.subplot(222)
        pp.plot(trng, rms_fsig)
        pp.plot(trng,sy.ones(slen)*noiseRMS)
        pp.plot(trng,sy.ones(slen)*peak)
        pp.plot((noiseRMStbeg, noiseRMStbeg), (-peak,peak) )
        pp.plot((noiseRMStend, noiseRMStend), (-peak,peak) )
        pp.xlabel('Time (ms)')
        pp.title('SNR {:02.3g} attn {:g}'.format(snrdb, ref_attn))


        pp.subplot(223)
        pp.plot(trng,fsignal)
        pp.plot(trng,sy.ones(slen)*noiseRMS)
        pp.plot(trng,sy.ones(slen)*peak)
        pp.grid()
        pp.xlabel('Time (s)')
        pp.ylabel('Amplitude (AU)')
        # pp.title('Filtered signal with max and RMS noise.')

        print( abs(max(fft.fft(fringe_signal))/max(h)))
        pp.subplot(224)
        pp.plot(frng[show_rng], 20*sy.log10(abs(fft.fftshift(fft.fft(fringe_signal)))[show_rng]))
        pp.plot(frng[show_rng], 20*sy.log10((abs(fft.fftshift(h))* abs(max(fft.fft(fringe_signal))/max(h)))[show_rng]))
        pp.grid()
        pp.xlabel('Frequency (Hz)')
        pp.ylabel('Power (dB)')
        pp.title('Filter vs Signal response')

        pp.draw()
        # pp.show(block=False)
        p1.wait_for_keys() # my event loop

        return {'feedbackPow':feedbackPow ,'ref_attn':ref_attn, 'snr_db': snrdb, 'snr_raw': snrr, 'rms_sig':rms_fsig, 'rms_ns':noiseRMS }

    def plot_a_line_segments(self, params):
        self.dbglg.info('Analysis')
        spatial_samples     = params.spatial_samples
        fdsh                = params.fdsh
        fds_len             = params.fds_len
        axial_resolution    = params.axial_resolution

        # this code fragment was supposed to provide a particular key control.
        # But right now we do not use it and it may realy only be useful for analysis plotting.
        ylim = [1e-1,1e5]
        evtpar = {'do_jmp':False, 'ylim':ylim}
        def key_pressed(e,par):
            # print('key: '+ str(e.key))
            if e.key == 'n':
                par['do_jmp'] = True
            if e.key == 'up':
                par['ylim'] = [par.get('ylim')[0]*2,par.get('ylim')[1]*2]
            if e.key == 'down':
                par['ylim'] = [par.get('ylim')[0]/2,par.get('ylim')[1]/2]

        fig1 = pp.figure('A-line segments')
        self.dbglg.info('spatial samples: '+str(spatial_samples))

        prev_ylim = 0

        start_plot_from = 0
        for pos in range(start_plot_from,fds_len): # process each mirror position
            a_line = sy.zeros(spatial_samples)
            for o in range(self.max_orders): # process each order of reflection
                # The data for each order must be summed in one segment now.
                # For single mirror reflections
                raw_a_line = fdsh[o,pos]
                # todo: 0 Can we detect when the orientation of the mirror scan is the opposite way ?
                raw_a_line = ny.fliplr( [raw_a_line])[0]
                # The axial_resolution is arbitrary and can be chosen as high as the actual sample rate
                # of the raw signal. But often the details are of limited value and axial_resolution should
                # not much more than the lateral resolution.
                new_sampling_rate = (o + 1) * axial_resolution
                a_line_segment = sg.resample( raw_a_line, new_sampling_rate)
                # todo: -1 The amplitude correction must be based on the expected DC offset.
                # The total DC does not help here, but each order has practically an own DC which would determine
                # the total intensity.
                # a_line_segment = (o + 1) / 2 * a_line_segment

                # The new_sampling_rate for each order determines a particular begin and end-position
                # within the A-line depending on the order.
                segment_start = o * self.odisp
                segment_end = o * self.odisp + (o + 1) * axial_resolution


                # Plot the step by step assemblance of one order each.
                # That means we see how each order segment is added to a full A-line.
                pp.subplot(211)
                # pp.plot(spatial_dim, sumdata)
                # pp.plot(sy.arange(segment_start, segment_end), a_line_segment)
                # pp.plot(range(spatial_samples), sy.ones(spatial_samples)*sy.nan)
                pp.plot(range(segment_start,segment_end),a_line_segment+o*500)
                # pp.plot(a_line)
                # ylim_max = max(pp.gca().get_ylim())
                ylim_max = max(a_line_segment)+self.max_orders*500
                if prev_ylim < ylim_max:
                    prev_ylim = ylim_max
                pp.ylim((0, prev_ylim))
                pp.xlim((0,spatial_samples))
                pp.title('A-segment before and after addition.')
                pp.draw()
                # self.pause(.1)
                a_line[segment_start : segment_end] += a_line_segment

            pp.subplot(212)
            pp.cla()
            pp.plot(a_line)
            pp.ylim([0,2500])
            pp.draw()

            pp.pause(.01)
            pp.subplot(211)
            pp.cla()

    def plot_a_line_profile(self, params):
        self.dbglg.info('Analysis.')

        pos             = params.pos
        a_line          = params.a_line
        a_line_segment  = params.a_line_segment
        segment_start   = params.segment_start
        segment_end     = params.segment_end
        gauss_gain      = params.gauss_gain

        show_position = 50  # show only profile at one position
        # Analyze the characteristics of compensation of the order segments
        # using a gaussian window at their side regions.
        if pos == show_position:
            plt = BasePlot()
            pp.figure('Sum Data gauss window')
            pp.plot(sy.log10(a_line))
            pp.hold(True)
            pp.plot(range(segment_start, segment_end), sy.log10(a_line_segment))
            pp.plot(range(segment_start, segment_end),
                    sy.log10(gauss_gain - gauss_gain * self.gauss_window(len(a_line_segment))))
            pp.ylabel('Log10 scaling')
            pp.xlabel('sample points')
            pp.title('Show with gauss window.')
            pp.pause(0.1)
            pp.draw()
            plt.wait_for_keys()

            pp.figure('Sum Data')
            pp.cla()
            pp.plot(sy.log10(a_line))
            pp.title('Log of sum data')
            pp.pause(0.1)
            pp.draw()
            plt.wait_for_keys()

class ImageProcessing(SignalConfig):
    '''This class is considered as a data research facility and may not meet user friendliness.\n
    This class is intended to load MRO data, such as mirror or real scans and perform a variety of
    processing steps in no particular order.

    Usage example:\n
    1) set the file name of the recorded data in self.fname\n
    2) If you do not generate a look-up table you can load a previously generated one by setting
        self.fname_lut\n
    3)
    '''
    xw_dim = None
    xh_dim = None
    def __init__(self, sigConf):
        self.initSigConf(sigConf)

    def replace_param_marker(self, imgfname, param_str):
        """

        :param imgfname:
        :param param_str:
        :return:
        """

        split_name = imgfname.split('#params#')
        name_prefix = split_name[0]
        name_postfix = split_name[1]

        return name_prefix + param_str + name_postfix


    def inc_img_counter(self, imgfname ):
        '''Set a incremental counter value in the filename.
        Attach some generic counter value into the filename for repeated processing steps
        to retain previous image processing steps.'''
        import os

        # print('imgfname',imgfname)
        pathFname, fname_ext = os.path.splitext(imgfname)
        cnt = 0

        # get counter if exist
        if 'rimg' in pathFname:
            prefix, counter = pathFname.split('_rimg')
            type = '_rimg'
            name_next_counter = '{}{}{}{}'.format(prefix, type, self.timeStamp, fname_ext)
        elif 'pimg' in pathFname:
            prefix, counter = pathFname.split('_pimg')
            type = '_pimg'
            name_next_counter = '{}{}{}{}'.format(prefix, type, self.timeStamp, fname_ext)

        # if len(counter) < 1:
        #     # attach new counter if new
        #     name_next_counter = '{}{}{}{}'.format(prefix, type, '000' ,fname_ext)
        #     # check if file with counter exists? Should actually not.
        #     while os.path.exists(name_next_counter):
        #         cnt += 1
        #         name_next_counter = '{}{}{:03d}{}'.format(prefix,type, cnt ,fname_ext)
        # else:
        #     # replace with incremented counter
        #     name_next_counter = '{}{}{}{}'.format(prefix,type, counter ,fname_ext)
        #     cnt = int(counter)
        #     # check if file with counter exists? Should actually not.
        #     while os.path.exists(name_next_counter):
        #         cnt += 1
        #         name_next_counter = '{}{}{:03d}{}'.format(prefix,type, cnt ,fname_ext)


        # print('return imgfname', name_next_counter)
        return name_next_counter


    def A_line_assembler(self, sumOrAdjoin = 'adjoin', axial_resolution = None, revert_segments=False, volume=True):
        '''
        Reconstruct a full A-line assembled of all the order of reflections.
        :param sumOrAdjoin: Can be 'adjoin' or 'sum'.
        :param axial_resolution: set the resolution used in the reconstructed image
        :param revert_segments: If true revert the segment alas forward scan or reverse scan.
        :return: self
        '''
        assert 'adjoin' in sumOrAdjoin or 'sum' in sumOrAdjoin, 'The parameter <sumOrAdjoin> currently takes only ''adjoin'' or ''sum'', given was ''{}'''.format(sumOrAdjoin)
        self.sumOrAdjoin = sumOrAdjoin

        odisp = self.odisp # preserve active configuartion and ignore stored value!
        self.dbglg.info('load file: '+self.name_hlb)
        # self.restore_processing_state(self.name_hlb)
        self.hilbert_data_fw = self.restore_aline_data(self.name_hlb)
        self.dbglg.warning('use odisp from config: {}'.format(odisp))
        self.odisp = odisp
        fringe_data_hilbert = sy.array(self.hilbert_data_fw)

        self.dbglg.info('type: ' + str(type(fringe_data_hilbert)))
        self.dbglg.info('Using full amount of samples! '+str(sy.shape(fringe_data_hilbert)))

        fds_len = len(fringe_data_hilbert[0])

        # todo:1 find adaptive control for order segment size and alignment.
        # depending on the dispersion this can be different for each order
        # reducing significance: Although, of interest it does not essentially reveal more information.
        # Although, it can reduce some edge artefacts.

        # done:2 It may be useful to correct for each order separately (see comment)
        # This should be done with the right calculation of the total scan depth, in which case the true depth and
        # the lateral dimension is calculated and used for plotting.

        # odisp should be matched to the actual pixel step width to obtain a diagonal line
        # without additional resampling.


        # was some resolution in the parameter?
        if not axial_resolution:
            axial_resolution = self.axial_img_res

        # Prepare total length of A-line:
        spatial_samples = (self.max_orders * self.odisp + (self.max_orders + 1) * axial_resolution)
        spatial_range = sy.arange(spatial_samples)
        #todo: naming redundancy
        spatial_dim = spatial_range # this relates to 1 or 1 mm

        imgmatrix = [] # 2D array of a B-frame or Calibration line

        # for the purpose of visualization of the single order elements the segments are processed
        # but kept in separate array elements for plotting them individually

        #analysis
        class params(object):pass
        params.spatial_samples = spatial_samples
        params.fdsh = fringe_data_hilbert
        params.fds_len = fds_len
        params.axial_resolution = axial_resolution
        # self.plot_a_line_segments(params)

        # return some explanation in case we want to use more orders than we had processed before.
        assert sy.shape(fringe_data_hilbert)[0] >= self.max_orders, 'You may need to run filtering again.'

        if 'sum' in self.sumOrAdjoin:
            self.dbglg.info('Perform summing of overlapping signals!')
        if 'adjoin' in self.sumOrAdjoin:
            self.dbglg.info('Cut and adjoin overlapping signals!')

        for pos in range(fds_len):

            # Add all orders into one A-line
            a_line = sy.zeros(spatial_samples)
            for o in range(self.max_orders):
                raw_a_line = fringe_data_hilbert[o,pos]
                if revert_segments:
                    # fringe_data_hilbert = self.revert_segments(fringe_data_hilbert)
                    raw_a_line = ny.fliplr([raw_a_line])[0]
                new_sampling_rate = (o + 1) * axial_resolution
                a_line_segment = sg.resample( raw_a_line, new_sampling_rate)
                # a_line_segment = (o + 1) / 2 * a_line_segment # todo -2: why do we do this here ???

                # todo -1: of coures this way the start is placed somewhat further and the end even further.
                # todo -1: But it should be moved at the end the same distance. Can be corrected.
                # the actual length
                segment_start = o * self.odisp
                segment_end = o * self.odisp + (o + 1) * axial_resolution

                gauss_gain = 1.0
                def normalize_segment(a_line_segment, gauss_gain):
                    """
                    Normalize_segment is flattening the response of an order segment along the z depth.
                    In theory, the confocal parameter plus a square law change of intensity vs z depth occurs.
                    In practise some curvature of the intensity profile can be observed.

                    Depending on the original characteristics of the signal the curvature of intensity change
                    may be more visible and this normalization can improve the overlapping characteristics.
                    In other cases this normalization may not improve too much.
                    :param a_line_segment:
                    :param gauss_gain:
                    :return:
                    """
                    # invert gauss_window and shift by +1 and apply to a_line_segment
                    return a_line_segment * (gauss_gain - gauss_gain * self.gauss_window(len(a_line_segment),5,10) + 1)

                def align_segments(order):
                    """
                    Depending on the order the shift increases.
                    Consequently we can somewhat compensate for a linear dispersion meaning,
                    the actual position of each peak is somewhat more delayed than predicted by
                    mere theoretical consideration.
                    :param order:
                    :return:
                    """
                    return int(order * self.pixel_shift)

                def sum_segments(a_line, a_line_segment, segment_start, segment_end, gauss_gain):
                    shift = align_segments(o)
                    a_line[segment_start + shift : segment_end] += a_line_segment[ shift:]
                    # a_line[segment_start : segment_end] += normalize_segment(a_line_segment, gauss_gain)
                    return a_line

                if 'sum' in self.sumOrAdjoin:
                    a_line = sum_segments(a_line, a_line_segment, segment_start, segment_end, gauss_gain)

                def adjoin_segments(a_line, a_line_segment, segment_start, segment_end):
                    '''For adjoining the order segments it is desirable to chose where to place the
                    cut-off to obtain the region with best intensity.
                    I.e. if no shift is given the next order starts with some reduced intensity but
                    is cut-off by the next order before reaching the best intensity.
                    So, shifting it by a few pixel we get an improved calibration line.
                    The pixel shift is scaled by the order such that shift = o * pixel_shift'''


                    def correct_drop_off(o, from_order = 4):
                        # drop-off correction to match the intensity change over at the cut-off regions from one to the
                        # next order.
                        # Start the correction when the drop-off starts. I.e. if the focus of the lens generates an increase
                        # up to the 3rd order and the fourth starts to drop-off then set from_order = 4.

                        # For tuning set prev_ and next_apm = 1 and record the amplitude from the from_order and next_order,
                        # assign those amplitudes here.
                        # For fine adjustments change the exponent.
                        # The square law works reasonably well here, but it should be related to the 80/20 splitting ratio
                        # of the PM. I.e. 0.2 * order.
                        prev_amp = 1500
                        next_amp = 4500
                        if o >= from_order:
                            # The first and second exponent were found by successive approximation
                            # The first exponent determines how quickly the second next order increases and should be
                            # chosen such that the second next order is equal or less.
                            # The second exponent determines the overall amplitude of all subsequent orders.
                            A = 0.7; B = 1.5; C = 3.9
                            return A * o ** B/C
                        else:
                            return 1.0

                    shift = align_segments( o )
                    a_line[segment_start + shift : segment_end] = a_line_segment[shift:] # * correct_drop_off(o)

                # Segment_end should not be shifted here as this would actually displace the order segment!
                if 'adjoin' in self.sumOrAdjoin:
                    adjoin_segments(a_line,a_line_segment, segment_start, segment_end)


                class params(object):pass
                params.pos = pos
                params.a_line = a_line
                params.a_line_segment = a_line_segment
                params.segment_start = segment_start
                params.segment_end = segment_end
                params.gauss_gain = gauss_gain
                # self.plot_a_line_profile( params )

            imgmatrix.append(a_line)

        if volume:
            img_name = os.path.join(self.path, self.fname_save) + '_3Dset.pkl'
            self.name_rimg_subs = img_name
        else:
            param_str = '_'+self.filter_type.print_params() + '_o{:02g}'.format(self.max_orders)
            # img_name = self.replace_param_marker( self.name_rimg, param_str)
            img_name = self.inc_img_counter(self.name_rimg)
            self.name_rimg_subs = img_name

        self.imgmatrix = imgmatrix
        self.dbglg.info('save image matrix as: ' + img_name)

        del(fringe_data_hilbert)
        del(self.hilbert_data_fw)

        self.store_processing_state(img_name)
        # sy.save(self.name_rimg,imgmatrix)
        return self


    def transform_img(self, img):
        self.dbglg.info('img shape: '+str(img.shape))

        from skimage.transform import PiecewiseAffineTransform, warp
        tform = PiecewiseAffineTransform()
        # collecting points of curved layers

        # srct = sy.array([[0,7],   [20,6],   [50,5],   [75,5.5], [100,8.8], [125, 12.3],[150, 18.0],[200,32.6],  [250, 52.5]])
        # srcb = sy.array([[0,75.8],[20,73.5],[50,73.1],[75,74.7],[100,77.0],[125, 81.2],[150, 86.9],[200, 102.6],[250, 122.1]])
        srct = sy.array([[7,   0],[6,   20],[5,   50],[5.5, 75],[8.8, 100],[12.3,125],[18.0,150],[32.6,200],  [52.5,250]])
        srcb = sy.array([[75.8,0],[73.5,20],[73.1,50],[74.7,75],[77.0,100],[81.2,125],[86.9,150],[102.6,200],[122.1, 250]])
        srcb[:,0] = srcb[:,0] # add shape to the maximum of the y-dims of the image
        src = sy.concatenate((srct, srcb))
        print(src)
        # create a matrix corresponding to the anticipated alignment
        destt = srct
        destt[:,0] = destt[:,0].min() # use the min y value for all target locations to make it straight
        destb = srcb
        # 80 is for 10 orders !!!
        destb[:,0] = destb[:,0]+ 50 # add shape to the maximum of the y-dims of the image
        destb[:,0] = destb[:,0].min() # use linear position at the minimum pixels
        dest = sy.concatenate((destt, destb))
        print(dest)
        tform.estimate(src,dest)
        wimg = warp(img, tform, output_shape=img.shape)
        return wimg


    def add_colorbar(self, im, aspect=20, pad_fraction=0.5, **kwargs):
        """Add a vertical color bar to an image plot.
        http://stackoverflow.com/questions/18195758/set-matplotlib-colorbar-size-to-match-graph
        """
        from mpl_toolkits import axes_grid1
        divider = axes_grid1.make_axes_locatable(im.axes)
        width = axes_grid1.axes_size.AxesY(im.axes, aspect=1 / aspect)
        pad = axes_grid1.axes_size.Fraction(pad_fraction, width)
        current_ax = pp.gca()
        cax = divider.append_axes("right", size=width, pad=pad)
        pp.sca(current_ax)
        return im.axes.figure.colorbar(im, cax=cax, **kwargs)

    def calc_image_dimensions(self,imgmtrx, mm, x_range, ptype = None):
        ih = sy.shape(imgmtrx)[0] # original resolution based on steps (image height in pixels)
        iw = sy.shape(imgmtrx)[1] # resampled resolution
        # Dsouza, 2014: Dermascope guided ...
        total_scan_range = self.scan_range/2 * (1 + self.max_orders) + self.PMspacing * (self.max_orders - 1)

        # for the scan range normalize pixel value first as the depth is based on overlapping that is
        # not alone determined by the pixel steps.
        self.xh_dim = sy.arange(0,ih) / ih * total_scan_range * mm
        # for lateral range iw containst the total steps as such
        self.xw_dim = sy.arange(0,iw) * self.mirror_step_size * mm

        # overwrite with specified scan range e.g. for imaging
        if not x_range is None:
            self.xw_dim = x_range



    def process_img_array(self, imgfname, ptype='cal', title=None, lin_log='lin', profile_pos = None, x_range=None, run_all=False):
        '''
        For **imgfname** use 'last' to get the last raw assembled img matrix (rimg) or\n
        use **imgfname = int** (i.e. imgfname = 1) to select a raw assembled matrix.\n
        :param imgfname: filename of the image matrix
        :param ptype: string = 'cal'/'img' to switch labeling for plots from calibration line to images
        :param title: Additional string to add to the title
        :param lin_log: string = 'lin'/'log'
        :param profile_pos: Array of x locations to generate the profile plot. I.e. [0, 5, 10, 15, ...] or using range(0,300,5)
        :param x_range: This value overwrites the calculated range according to the mirror calibration.
        :return:
        '''
        bpp = BasePlot()

        # search_str = self.replace_param_marker(self.name_rimg, '*').split('.pkl')[0]+'???.pkl'
        search_str = self.name_rimg.split('.pkl')[0]+'*.pkl'
        # print(search_str)
        rimg_files = sorted(glob.glob(search_str))
        # rimg_files = glob.glob(self.path + '/d2t2MC-v2*_rimg*.pkl')
        # print(rimg_files)

        if type(imgfname) == str and imgfname == 'last':
            rimg_file = rimg_files[-1]

        elif type(imgfname) == int:
            print(rimg_files)
            print(len(rimg_files))
            print(len(rimg_files) >= 1)
            assert len(rimg_files) > 0, 'You must poosibly run A_line_assemble once.'
            assert len(rimg_files) >= imgfname, 'The selected processing step ({:03g}) appears not to exist.'.format(imgfname)
            rimg_file = rimg_files[imgfname]

        self.dbglg.info('load {}'.format(rimg_file))
        self.restore_processing_state(rimg_file)

        # if we really want a pure npy file lets write a function for exporting.
        imgmtrx = sy.array(self.imgmatrix).T

        a_lines_in_data = sy.shape(imgmtrx)[1]
        if profile_pos.stop > a_lines_in_data:
            profile_pos = range(profile_pos.start, a_lines_in_data, profile_pos.step)

        newimgname = self.inc_img_counter(rimg_file )

        self.new_processed_img_name = newimgname
        self.dbglg.info('max in img: ' + str(ny.max(imgmtrx)))
        self.dbglg.info('shape img (h,w): '+str(sy.shape(imgmtrx)))
        mm = 1000

        self.calc_image_dimensions(imgmtrx, mm, x_range, ptype = ptype)

        # set zero values in the image to 1.0
        # Those zero values cannot be calculated with log
        zero_slice = sy.where(imgmtrx == 0.0 )
        nan_slice = sy.where(imgmtrx == sy.nan)
        imgmtrx[ zero_slice ] = 0.1
        imgmtrx[ nan_slice  ] = 0.1

        # todo -1: profiling - is this valid?
        # self.dbglg.warning('Use internal profile normalization.')
        # myimg_sum = sy.sum(myimg, axis=1)
        # myimg = (myimg.T * 1/myimg_sum).T

        # Fill missing values with some level to indicate this in plot
        imgmtrx[ zero_slice ] = sy.median(imgmtrx)/10


        if ptype == 'cal':
            xlabel = 'Sample mirror positions $z_n$ (mm)'
            ylabel = 'Signal of mirror position (mm)'
        elif ptype == 'img':
            xlabel = 'Latteral dimension (mm)'
            ylabel = 'Depth (mm)'
        else:
            xlabel = 'x (mm)'
            ylabel = 'y (mm)'

        # create profile on linear data
        # p = sy.polyfit(sy.arange(ih)*self.step_size*mm, sy.log10(sy.sum(myimg, axis=1)),deg=22)
        # self.dbglg.info('profile fit params: '+str(p))
        # fitfun = sy.poly1d(p)
        # myimg_prof = fitfun(xh_dim)
        #
        # pp.plot(xh_dim, myimg[180])
        # pp.plot(sy.arange(ih)*self.step_size*mm, sy.log10(sy.sum(myimg, axis=1)))
        # pp.plot(xh_dim, myimg_prof)
        # pp.draw()
        # self.pause()
        # myimg[sy.where(myimg < 0)] = 0
        # myimg = myimg - ny.max(myimg)
        myimg_log = (sy.log10(abs(imgmtrx)))*20
        # myimg_logm = sy.mean(myimg_log,axis=0)
        # p = sy.polyfit(xh_dim, myimg_logm, 5)
        # fitfun = sy.poly1d(p)
        # myimg_lprof = fitfun(xh_dim)

        pp.rcParams['font.size']=16

        if lin_log == 'lin':
            plot_lin = True
        elif lin_log == 'log':
            plot_lin = False
        else:
            self.dbglg.info('lin_log = '+lin_log+' not found. Use log scale. (Available options: \'lin\', \'log\'.')
            plot_lin = False

        if not profile_pos:
            pos = 180
        else:
            pos = profile_pos

        def createfig(x,y,name):
            fig, ax = pp.subplots(num=name, figsize=(9,10))
            # fig = pp.figure(name,figsize=(9,10))
            fig.canvas.manager.window.move(x,y)
            return fig, ax

        def config_plot(im, cbl,vmin,vmax):
            '''
            Configure common plotting parameters.
            With the new matplotlib version 2.0.2 the colorbar becomes chunked in color-levels.
            https://stackoverflow.com/questions/13960480/matplotlib-colorbar-smoothness

            To avoid
            '''
            # pp.xlim((0, xw_dim.max()))
            # pp.ylim((xh_dim.max(), 0))

            # To achieve a smooth colorbar see link below
            # https://stackoverflow.com/questions/8342549/matplotlib-add-colorbar-to-a-sequence-of-line-plots
            # https://github.com/matplotlib/matplotlib/issues/3644
            # pp.colorbar(im)
            # sm = pp.cm.ScalarMappable(cmap=pp.get_cmap('Accent_r'), norm= pp.Normalize(vmin=vmin, vmax=vmax))
            # sm._A = []
            cb = self.add_colorbar(im)
            cb.set_label(cbl)
            pp.xlabel(xlabel)
            pp.ylabel(ylabel)
            pp.tight_layout()
            pp.grid(True, color='white')

        def savefig(figname):
            self.dbglg.info('savefig ' + figname)
            pp.tight_layout()
            pp.savefig(figname + '.png',bbox_inches='tight')
            pp.savefig(figname + '.pdf',bbox_inches='tight')

        def calc_RMS(profile_pos):
            """Currently only show RMS and median for one position,
            to confirm the relevance of the difference between RMS and median."""
            single_slice = 100

            profile = myimg_log[:, single_slice]
            # remove the peak and calculate the RMS
            max_val = ny.max(profile)
            peak_pos = sy.where( profile == max_val)[0]
            margin = 10
            profL = profile[52: peak_pos - margin]
            rmsL = ny.sqrt( ny.mean( ny.array( profL ) ** 2.0 ))
            end = -1
            rmsR = ny.sqrt( ny.mean(profile[peak_pos + margin: end] ** 2))
            pp.plot(profile,label='profile')
            pp.plot([peak_pos-margin, peak_pos-margin], [0,max_val])
            pp.plot([peak_pos+margin, peak_pos+margin], [0,max_val])
            pp.plot(ny.ones(len(profile))*rmsL,label='rmsL')
            pp.plot(ny.ones(len(profile))*rmsR,label='rmsR')
            rmsMedian = ny.median(profile)
            pp.plot(ny.ones(len(profile))*rmsMedian,label='median')
            self.dbglg.info('rmsR: '+str(rmsR))
            self.dbglg.info('median: '+str(rmsMedian))
            pp.legend()
            pp.draw()
            bpp.wait_for_keys()

        # calc_RMS(profile_pos = profile_pos)

        def plot_SNRs(profile_pos = profile_pos):
            """
            Calculate the SNR values based on the median and the maximum of the response peak.
            The median is the mean of the medians of all used signals.
            :return:
            """
            medianVals = []
            maxVals = []
            [medianVals.append( sy.median( myimg_log[:, slice])) for slice in profile_pos]
            medianAvg = sy.mean( medianVals )
            self.dbglg.info('medianAvg: '+str(medianAvg))

            [maxVals.append( ny.max( myimg_log[:, slice])) for slice in profile_pos]
            self.dbglg.info('maxVals: '+str(maxVals))

            # if we take the log data then it is a subtraction, and we multiply by 20 already!
            # SNR_Vals = ( maxVals - medianAvg )
            intensity = maxVals
            z_pos = sy.linspace(0, 0.7, len(intensity))
            pp.plot(z_pos, intensity, 'o')
            pp.ylim([-10,100])
            pp.grid(True)
            pp.xlabel('z-position (mm)')
            pp.ylabel('Intensity (dB$_log_{20}$)')
            pp.title(title+'\nIntensity profile')
            # plt.wait_for_keys()
            snrfname = newimgname.split('.pkl')[0] + '_intensity_prof'
            # ny.save(snrfname + '.npy', {'z_pos':z_pos, 'intensity_Vals':intensity})
            with open(snrfname+'.pkl','wb+') as fid:
                pickle.dump({'z_pos':z_pos, 'intensity_Vals':intensity},fid)
            savefig( snrfname )

        plot_SNRs(profile_pos)

        def plot_PSFs():
            """Measure the PSF of a peak."""
            pass

        def plot_profile_locs(ax,xpos):
            """Overlay a dashed line there where we get the profile from"""
            ax.plot((xpos,xpos),(0,self.xh_dim.max()),lw=1,ls='dashed')

        def plot_profile(scaling = lin_log):
            fig1 = pp.figure('Profile',figsize=(10,7))
            fig1.canvas.manager.window.move(1200,100)

            if plot_lin:
                for slice in profile_pos:
                    pp.plot(self.xh_dim, (imgmtrx[:,slice]))
                # pp.plot(xh_dim,myimg_sum)
                # pp.plot(xh_dim,myimg_prof)
                # pp.plot(xh_dim, myimg[pos]-myimg_prof)
                pp.ylabel('Intensity (arbitrary)')
            else:
                # pp.plot(xh_dim,sy.sum(myimg_log[pos:pos+10]/10, axis=0))
                for slice in profile_pos:
                    pp.plot(self.xh_dim, (myimg_log[:,slice]))

                # pp.plot(xh_dim,myimg_logm)
                # pp.plot(xh_dim,myimg_lprof)
                # pp.plot(xh_dim, myimg_log[pos]-myimg_lprof)
                pp.ylabel('Intensity (Log)')

            # pp.ylim([-5,0])
            um = 1e6
            pp.title('{}\n{:3.0f} $\mu m$ spacing'.format(title, self.PMspacing * um)) #self.myname))

            pp.xlabel('Depth (mm)')
            pp.grid()
            pp.ylim([-10,100])
            pp.tight_layout()
            pp.draw()

            # todo: -1 change this to the new naming processing.
            # The position data may be better allocated into the pkl file and the log file by now
            # together with filenames generated.
            figname = newimgname.split('.pkl')[0]
            # figname = figname + '_prof_pos'+'{:g}'.format(pos[0])+'-'+'{:g}'.format(pos[-1])
            figname = figname + '_prof'
            self.dbglg.info('savefig '+ figname)
            savefig(figname)

            plot_data = pp.gca().get_lines()
            sy.save(figname.split('.pkl')[0]+'.npy', plot_data)

        plot_profile()

        def plot_gray(scaling=lin_log, vmin=20, vmax=80):
            fig2, ax = createfig(0,0,'Grey scaled')
            xpos = sy.array(pos) * self.mirror_step_size * mm
            if scaling == 'lin':
                im = ax.imshow(imgmtrx, cmap='Greys_r', interpolation='None',
                           extent=[0, self.xw_dim.max(), self.xh_dim.max(), 0],
                           vmin = -400,
                           vmax = 1000
                )
                plot_profile_locs(ax,xpos)
                ax.set_ylim([0,0])
                # force to show only data space, and do not enhance with empty space
                cbl = 'Intensity (linear scale)'
                config_plot(im, cbl)
                figname = newimgname.split('.pkl')[0] + '-lin-grey'

            elif scaling == 'log':
                im = ax.imshow(myimg_log, cmap='Greys_r', interpolation='None',
                               extent=[0,self.xw_dim.max(), self.xh_dim.max(), 0],
                               vmin= vmin, vmax=vmax,
                               )
                plot_profile_locs(ax,xpos)
                ax.set_ylim((self.xh_dim.max(),0)) # to avoid white regions
                ax.set_xlim((0,self.xw_dim.max())) # to avoid white regions
                cbl = 'Intensity (log$_{10}$ scale)'
                config_plot(im,cbl,vmin=vmin,vmax=vmax)
                figname = newimgname.split('.pkl')[0] + '-log-grey'

            pp.title(title)
            pp.pause(0.1)
            pp.draw()
            savefig(figname)

        plot_gray()

        def avg_tape_structure():
            fig4, ax = pp.subplots(1,1)
            img_mean = sy.mean(myimg_log,axis=1)
            pp.plot(self.xh_dim, img_mean)
            pp.plot(self.xh_dim, sy.ones(len(self.xh_dim)) * sy.median(img_mean))
            pp.title('Mean of tape structure')
            pp.tight_layout()
            pp.savefig(newimgname.split('.pkl')[0] + '_structure.pdf')
            pp.savefig(newimgname.split('.pkl')[0] + '_structure.png')
        # avg_tape_structure()

        def plot_color(scaling = lin_log, vmin=20, vmax=80):
            fig3, ax = createfig(700,0,'Color scale')
            if scaling == 'lin':
                im = pp.imshow(imgmtrx , cmap='Accent_r', interpolation='None',
                          extent=[0, self.xw_dim.max(), self.xh_dim.max(), 0],
                               vmax=1000,
                               vmin=-400
                               )
                figname = newimgname.split('.pkl')[0] + '-lin-col'
                cbl = ('Intensity (linear scale)')
                config_plot(im,cbl)

            elif scaling == 'log':
                # https://github.com/matplotlib/matplotlib/issues/881
                # http://scipy-cookbook.readthedocs.io/items/Matplotlib_ColormapTransformations.html
                # https://matplotlib.org/examples/pylab_examples/custom_cmap.html
                # https://matplotlib.org/examples/api/colorbar_only.html
                # https://sites.google.com/site/theodoregoetz/notes/matplotlib_colormapadjust
                # Accent, CMRmap, Dark2
                im = pp.imshow(myimg_log , cmap='CMRmap', interpolation='None',
                               extent=[0, self.xw_dim.max(), self.xh_dim.max(), 0],
                               # vmax=vmax,
                               # vmin=vmin,
                               )
                cbl = 'Intensity (log$_{10}$ scale)'
                config_plot(im,cbl,vmin=10,vmax=80)
                figname = newimgname.split('.pkl')[0] + '-log-col'

            pp.title(title)
            savefig(figname)

        plot_color()

        # save only parameters and use the parameter file to manage the counter,
        # to be able to generate multiple output files.
        # In some way the data are available in the log file, but having some redundance
        # is not too bad either.
        del(self.imgmatrix)
        if 'reverse_data' in self.__dict__:
            del(self.reverse_data)
        if 'fitted_data' in self.__dict__:
            del(self.fitted_phase)

        self.list_params()
        self.store_processing_state(newimgname)

        # de-warp
        pp.show(block=False)
        if not run_all:
            bpp.wait_for_keys()
        else:
            pp.pause(0.1)
        #
        pp.close('all')
        # pp.show()

        return self

    def fillNaN_Zeros(self, imgmtrx):
        """
        The image data contain possibly values of type NaN and zero.
        Those values cannot be used for calculating a logarithmic scale
        and even if, the color value would be unpredictable.
        So all those missing regions are filled with an arbitrary median value,
        that is some area with no data.
        :param imgmtrx:
        :return:
        """
        # set zero values in the image to 1.0
        # Those zero values cannot be calculated with log
        zero_slice = sy.where(imgmtrx == 0.0 )
        nan_slice = sy.where(imgmtrx == sy.nan)
        imgmtrx[ zero_slice ] = 0.1
        imgmtrx[ nan_slice  ] = 0.1

        # todo -1: profiling - is this valid?
        # self.dbglg.warning('Use internal profile normalization.')
        # myimg_sum = sy.sum(myimg, axis=1)
        # myimg = (myimg.T * 1/myimg_sum).T

        # Fill missing values with some level to indicate this in plot
        imgmtrx[ zero_slice ] = sy.median(imgmtrx)/10
        return imgmtrx

    def processVolumeSlice(self, binfile, comment = '', x_range = None):

        imgmtrx = sy.array(self.imgmatrix).T
        imgmtrx = self.fillNaN_Zeros(imgmtrx)
        myimg_log = (sy.log10(abs(imgmtrx)))*20

        mm = 1000
        self.calc_image_dimensions(imgmtrx, mm, x_range=x_range)

        def plot_gray(scaling='log', vmin=10, vmax=70):
            self.dbglg.info('(vmin, vmax)= ('+str(vmin)+', '+str(vmax)+')')
            # fig, ax = pp.subplots()

            if scaling == 'log':
                fname = os.path.splitext(binfile)[0]
                fname = fname+'{}.png'.format(comment)
                self.dbglg.info('save image as '+str(fname))
                scipy.misc.toimage(myimg_log,cmin=vmin,cmax=vmax).save(fname)
                # im = ax.imshow(myimg_log, cmap='Greys_r', interpolation='None',
                #                extent=[0,self.xw_dim.max(), self.xh_dim.max(), 0],
                #                vmin=-80, vmax=-10
                               # vmin= vmin, vmax=vmax
                               # )
            # ax.get_xaxis().set_visible(False)
            # ax.get_yaxis().set_visible(False)
            # pp.axis('off')
            # pp.tight_layout()

            # pp.savefig(binfile.split('.bin')[0]+'c{:03.2f}{}.png'.format(factor,comment),bbox_inches='tight',pad_inches=0)

            # pp.close(fig)

        plot_gray()

        del(self.imgmatrix)
        if 'reverse_data' in self.__dict__:
            del(self.reverse_data)
        if 'fitted_phase' in self.__dict__:
            del(self.fitted_phase)
        # self.list_params()
        # self.store_processing_state(newimgname)
        return self

#####
# List of todos
#####

# todo:1 create a function that measures SNR
# In the future it is more important to have multiple points to measure the quality of the signal.
# todo:1 measure PSF
# increased from 2 to 1 as it may become more significant soon.
# we can calculate and measure the PSF. Apparently we appear to observe a broading towards deeper regions
# meaning the focus is at the first order.
# todo:1 before summing into A-line allow for further corrections. (Prio:High)
# Reduced to prio 1, because we have a generation of a profile based on mirror data.
# However, this is not perfect as the low resolution at this stage creates major offsets of other data
# that are not exactly matching.
# Further, more methods can be employed for further corrections such as the spatial alignment based on
# correlation of overlapping regions.
# todo:1 keep array with spatially corrected signals in plot_resampled_orders (Prio:Medium)
# currently we assemble an A-line directly with fixed constants to resample to the right length.
# This works however only, if the signal behaves well and is not distorted.
# Otherwise it may also be able to extract furthe information from the spatial data.

# todo:2 identify extinction and shift (Prio: Low)
# In the sum function it may be useful to identify when the signal becomes to little, which can
# cause a discontinuity in the phase.
# If we can identify extinction than a shift of one summand can avoid extinction while the signal
# phase continuity remains preserved.
# The error of change in phase is reduced by the averaging effect of the sum.

# todo:2 set voltage scale on signal
# This is mainly required for SNR
# ...

# todo:2 in linearize_data interpolation must be protected against nan values
# todo:2 create a fitting function Gauss * Sin that can be fitted onto multiple peaks.
# todo:2 generate multiple Gaussion with non-linear phase, extract the phase, linearize and show
# the recovered input signals for each wave form with error evaluation.
# It is perceivable to simulate the non-linear signal directly and try to fit on it.
# Then the best fit should provide all positional parameters required.
# But it also needs some more work to test and build the framework.
# todo:2 what is faster? boxcar on all samples or boxcar low samples and fitting?
# If the boxcar must run over all samples to create a mean it takes at least N steps for n samples.
# With less steps of N, fitting requires possibly little steps if we expect a possible shape and
# use then the model function to optain the full sample again.

# todo:2 The boxcar cannot find points for the very first samples (Prio: Low)
# this is a typical problem and a variety of other algorithm try to tackle this.
# However, for the moment this has little significance as the side regions may be cut-off.

# todo:2 But also applying the boxcar on the sum signal may be interesting. (Prio: Low)
# done:0 plot orders as separate segments above each other to evaluate alignment.
