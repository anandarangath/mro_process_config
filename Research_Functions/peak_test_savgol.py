
'''
Testing filtering with the savgol filter ...

'''
from scipy import *
from scipy.signal import savgol_filter
from scipy.signal import find_peaks
from matplotlib.pyplot import *

f = 8
w = 2*pi*f
SP = 1000
clear_sig = sin(linspace(0,w,SP) )
noise_sig = (rand(SP) * 1 - 0.5) + clear_sig
filter_sig = savgol_filter( noise_sig, window_length=151, polyorder=6)
peaks, _ = find_peaks( filter_sig, distance=75)
print(peaks)

print(filter_sig[peaks])

plot(  clear_sig, label='orig')
plot(  noise_sig, label='noise' )
plot(  filter_sig,label='savgol')
plot(  peaks, filter_sig[peaks], 'x')
legend()
show()