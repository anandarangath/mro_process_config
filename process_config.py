"""
Created on 01/04/2019; 13:50
@author: Kai Neuhaus,edits anandarangath
@email: kaineuhaus.mail@gmail.com, k.neuhaus2@nuigalway.ie
IDE: PyCharm Community edition

Process configuration example file.

"""
from typing import Type

import numpy as ny
import scipy as sy
from analyze_signal import *
import logging
import time
import sys
import subprocess as sp
import os
import re
import pathlib


class Process_config():
    timeStamp=time.strftime('%Y%m%d-%H%M%S')

    sigConf = SignalConfig()
    def config_logger(self, data_path, object):
        '''
        :param self:
        :return:
        https://docs.python.org/3/howto/logging-cookbook.html
        https://docs.python.org/2.4/lib/minimal-example.html
        https://docs.python.org/3/library/logging.html
        '''
        object.parent_git_version = sp.check_output(['git', 'rev-parse', 'HEAD'])

        logging.basicConfig(level=logging.CRITICAL)
        assert os.path.exists(data_path) , 'Please check the path: {:s}! This appears not to exist.'.format(data_path)
        dataFileHandler = logging.FileHandler(data_path + 'program-' + self.timeStamp + '.log',mode='a+')
        debugFileHandler = logging.FileHandler(data_path + 'program-' + self.timeStamp + '.log',mode='a+')
        debugStreamHandler = logging.StreamHandler(stream=sys.stdout)

        dataFormatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(funcName)s:%(lineno)d - %(message)s')
        debugFormatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(funcName)s:%(lineno)d - %(message)s')

        debugStreamHandler.setFormatter(debugFormatter)
        debugFileHandler.setFormatter(debugFormatter)
        dataFileHandler.setFormatter(dataFormatter)

        object.dbglg = logging.Logger('PRG')  # get logger inherits and creates dublicate
        object.dbglg.setLevel(logging.INFO)  # logging.DEBUG == 10
        object.dbglg.handlers = []
        object.dbglg.addHandler(debugStreamHandler)
        object.dbglg.addHandler(debugFileHandler)

        object.datlg = logging.Logger('DAT')
        # object.datlg.setLevel(10)
        object.datlg.addHandler(dataFileHandler)

    def default_configuration_setup(self,path):
        self.sigConf.timeStamp = self.timeStamp
        self.sigConf.path = path
        # We need to be aware here that the static definition of items do not propagate through
        # to the child class that it is inheriting SignalConfig.
        # However, the reason is that managing the values as semi dictionary is somewhat more
        # convenient if using it in the member functions.

        self.config_logger( data_path = self.sigConf.path, object = self.sigConf)

        self.sigConf.wavelength = 1330e-6
        self.sigConf.scan_freq = 150.0# Hz of the scanning mirror with piezo
        self.sigConf.sample_rate = 10e6  # 1 MHz
        self.sigConf.sample_interval = 1/self.sigConf.sample_rate #ns sample time of the digitizer, possibly
        self.sigConf.mirror_step_size = 3.0e-6 # um step width/size for mirror data (guess , don't know)

        self.sigConf.scan_range = 71e-6 # estimated or measured scan range of the scanning mirror (throw)
        self.sigConf.throw = self.sigConf.scan_range # call find_filter_parameters
        # sigConf.PMspacing = 60e-6 # this value is not exactly the spacing than more like spacing times orders
        self.sigConf.PMspacing = 89e-6 # this value is not exactly the spacing than more like spacing times orders

        # sigConf.mirror_step_size = 5e-6  # um step width/size for mirror data (guess , don't know)
        # sigConf.scan_range = 95.0e-6  # estimated or measured scan range of the scanning mirror (throw)
        # # sigConf.throw = self.sigConf.scan_range  # call find_filter_parameters
        # sigConf.PMspacing = 140e-6
        self.sigConf.max_orders = 14
        # self.sigConf.peak_list = sy.array([62, 123, 183, 245, 306, 369, 430, 490, 555, 621])
        # self.sigConf.peak_list = sy.array([62, 125, 190, 253, 316, 380, 445, 505, 570, 640])
        # self.sigConf.peak_list = sy.array([57, 114, 171, 228, 285, 342, 399, 456, 513, 570])
        # 57, 114, 171, 228, 285, 342, 399, 456, 513, 570
        # self.sigConf.peak_list = sy.array([53, 106, 159, 212, 265, 318, 371, 424, 477, 530, 583, 636]) # DATA_25_06_2019/2/
        self.sigConf.peak_list = sy.array([58,116,172,227,283,340,395,454,509,567,622,679])
        # self.sigConf.peak_list = sy.array([58, 116, 173, 231, 291, 349, 407, 464, 524, 582, 683, 679])
        self.sigConf.peak_list = sy.array([57,115,173,  231,  289,  348,  406,  464,522,580,  638, 697,755,815])
        # self.sigConf.peak_list = sy.array([58, 117, 176, 234, 293, 351, 410, 469, 527, 587, 644, 703, 762, 822])
        # *****
        # The subsequent parameters can or must be determined using analysis steps with this processing framework.
        # I.e. start convert_mro_bin with do_plot=True
        # *****
        self.sigConf.mroDtype = '>u2'
        # The fitting for the phase needs to sum multiple signals
        # You need to check what signals create the best fitting.

        # All parameters below require to call the A_line_assembler again
        # 1) start with axial_img_res = 10 or so
        #    and odisp = 10.
        # 2) If the order are cut-off decrease axial_img_res by 5 or increase odisp by 10.
        # 3) Increase odisp by 10 or any other value to merge the segments.
        # 4) Increase axial_img_res if cut-off in spaced orders is still visible.
        self.sigConf.axial_img_res = 30  # order slope in image
        self.sigConf.odisp = 60 # order spacing and slope in image
        self.sigConf.pixel_shift =7  # set this 0 initially
        self.sigConf.filter_type = FilterType()
        self.sigConf.filter_type.name = 'gauss'
        # sigConf.filter_type.pb = 0.00050
        # sigConf.filter_type.sb = 0.00250
        # sigConf.filter_type.pb = 0.0010
        # sigConf.filter_type.sb = 0.0050
        self.sigConf.filter_type.gbw = 14

        # The prameters are relevant for the the filter and use redundant names
        self.sigConf.mro_segment_len = 20000
        self.sigConf.sample_len = self.sigConf.mro_segment_len # number of samples; call find_point_of_returns to obtain this number

        return self.sigConf

    def psmro_MD(self, call_direct=False, setName=None, fname_scan=None):
        '''
        Calibration
        :param call_direct:
        :param setName:
        :param fname_scan:
        :return:
        '''
        self.sigConf.p_sig_start = 40  # call sum_signals( ..., do_plot=True)
        self.sigConf.p_sig_start = 40
        self.sigConf.p_sig_use = range(0, 20, 2) # [42,44,46,48,50,52,54,56,58,60,62,64,67,69] #[40, 45, 57, 64] # steps to use for summing signals 1st order


        forward_start   = 0
        signal_len = 67000
        offset = 1000 #1500
        forward_end     = (signal_len - offset)//2
        reverse_start   = (signal_len - offset)//2 # reverse data are currently not used for processing
        reverse_end     = signal_len - offset

        data_folder_name = r"E:\MRO DATA\11_02_2020\OD_2_sensitivity\2" # must have final '/'
        self.sigConf = self.default_configuration_setup(path=data_folder_name) # looks for data.bin
        self.sigConf.sigP = [forward_start, forward_end, reverse_start, reverse_end]
        self.sigConf.fname_save = "Anand_data"

        pproc = PreprocessSignal(self.sigConf)

        # convert bin data to npy
        self.sigConf = pproc.convert_mro_bin(bin_file=os.path.join(data_folder_name,"data.bin"), a_line_len=67000, offset=0, do_plot=False, start_plot=0)
        # fname must be None!
        self.sigConf = pproc.load_mro_data(fname =  None , do_plot=False, start_plot=0)

        lin = Linearize( self.sigConf )
        # self.sigConf = lin.pass_data_directly()
        self.sigConf = lin.sum_signals( step_mode = False, do_plot =False)
        self.sigConf = lin.fit_phase(do_plot=True) # create a phase fit and save a phs-file
        self.sigConf = lin.linearize_data(do_plotSteps = False, do_analyze=200) # create linearized data lin-file

        filt = Filtering( self.sigConf )
        filt.max_khz = 0.1
        # filt.find_filter_parameters( do_plot=True ) # Find the peaks for the filet

        self.sigConf = filt.filter_lin_data(do_window={'type':'Gauss'},
                                            filter_type=self.sigConf.filter_type,
                                            do_plot=False,
                                            plot_start_step=120)

        self.sigConf = filt.hilbert_data(instance=self, use_mp=False)

        imgProc = ImageProcessing(self.sigConf)
        sumOrAdjoin = 'sum'
        self.sigConf = imgProc.A_line_assembler(sumOrAdjoin=sumOrAdjoin,
                                                axial_resolution=self.sigConf.axial_img_res,
                                                revert_segments=False,
                                                volume=False)
        # imgProc.analyze_mro_npy(do_plot=True, analyze_data=0)
        self.sigConf = imgProc.process_img_array(imgfname='last',  # use 'last', a number 1,2, ..., or filename.
                                            ptype='cal',  # 'img' / 'cal'
                                            title='{} - {} - {}'.format(*re.split('/|\\\\',data_folder_name)[-3:]),
                                            lin_log='log',  # log / lin
                                            profile_pos=range(3, 500, 10))  # profiles (start, end, amount)

    def psmro_Bframe(self, call_direct=False, setName=None, fname_scan=None):
        '''
        B-frame processing
        :param call_direct:
        :param setName:
        :param fname_scan:
        :return:
        '''
        # Find this this length by trial and error on a mirror signal from the B-frame scan.
        # Try to image the mirror at a slight tilt to have moving orders required to see the turnaround points.
        a_line_len = 66675
        offset = 3075
        # These parameters are required for load_mro_data
        forward_start   = offset
        forward_end     = a_line_len//2 + offset
        reverse_start   = a_line_len//2 + offset
        reverse_end     = a_line_len + offset
        self.sigConf.sigP = [forward_start, forward_end, reverse_start, reverse_end]

        try:
            # If path and file is given as parameter, i.e. for LabView call
            pos_path = pathlib.WindowsPath(sys.argv[1]).as_posix()
            print(pos_path)
            data_folder_name = os.path.split(pos_path)[0]
            bframe_file = data_folder_name + '/' + os.path.split(pos_path)[1]
            self.sigConf.fname_save = os.path.split(pos_path)[1]  # this is the name the processed files are saved in
        except Exception as ex:
            print(str(ex))
            # Enable those two lines to give manual path and file to process
            data_folder_name = r"E:\MRO DATA\11_02_2020\B_frame_scotch_tape\1"
            bframe_file = os.path.join(data_folder_name,"B-frame_file_white_tape210019_000.bin")
            self.sigConf.fname_save = bframe_file.split('.bin')[0]  # this is the name the processed files are saved in

            # Set a specific name for the processed files (output file names)
            # self.sigConf.fname_save = "specific_file_name"

        self.sigConf = self.default_configuration_setup(path = data_folder_name)
        self.sigConf.build_filenames()

        pproc = PreprocessSignal(self.sigConf)

        # convert bin data to npy
        self.sigConf = pproc.convert_mro_bin(bin_file = bframe_file, a_line_len=a_line_len, offset=67000-0, do_plot=False, start_plot=2)
        # fname must be None!
        self.sigConf = pproc.load_mro_data(fname =  None , do_plot=False, start_plot=20)

        lin = Linearize( self.sigConf )
        # path_to_phase_data = r'C:\Users\Anand\Desktop\MRO_DATA_01_10_2019\1\mirror_data\anand_test_1_phs.pkl'
        path_to_phase_data = r'E:\MRO DATA\11_02_2020\A_line_calibration\1\Anand_data_phs.pkl'
        self.sigConf = lin.linearize_data(do_plotSteps = False,load_fitted_phase=path_to_phase_data, do_analyze=0) # create linearized data lin-file

        filt = Filtering( self.sigConf )
        filt.max_khz = 0.1
        # filt.find_filter_parameters( do_plot=True ) # Find the peaks for the filet

        self.sigConf = filt.filter_lin_data(do_window={'type':'Gauss'},
                                            filter_type=self.sigConf.filter_type,
                                            do_plot=False,
                                            plot_start_step=5)

        self.sigConf = filt.hilbert_data(instance=self, use_mp=False)

        imgProc = ImageProcessing(self.sigConf)
        sumOrAdjoin = 'sum' # 'sum' or 'adjoin'
        self.sigConf = imgProc.A_line_assembler(sumOrAdjoin=sumOrAdjoin,
                                                axial_resolution=self.sigConf.axial_img_res,
                                                revert_segments=True,
                                                volume=False)
        # imgProc.analyze_mro_npy(do_plot=True, analyze_data=0)
        self.sigConf = imgProc.process_img_array(imgfname='last',  # use 'last', a number 1,2, ..., or filename.
                                            ptype='img',  # 'img' / 'cal'
                                            title='{} - {} - {}'.format(*re.split('/|\\\\',data_folder_name)[-3:]),
                                            lin_log='log',  # log / lin
                                            x_range=sy.arange(0,2),
                                            profile_pos=range(20, 500, 10))  # profiles (start, end, amount)



pc = Process_config()
# pc.psmro_MD(call_direct=True)
pc.psmro_Bframe(call_direct=True)
